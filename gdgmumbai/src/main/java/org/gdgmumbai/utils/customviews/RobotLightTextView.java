package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import org.gdgmumbai.utils.RobotoLightFontItalicSingleton;
import org.gdgmumbai.utils.RobotoLightFontNormalSingleton;

/**
 * Created by Ammar Githam on 11-09-2014.
 */
public class RobotLightTextView extends AppCompatTextView {

    public RobotLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotLightTextView(Context context) {
        super(context);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {

        if (!isInEditMode()) {
            switch (style) {

                default:
                case Typeface.NORMAL:
                    setTypeface(RobotoLightFontNormalSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.ITALIC:
                    setTypeface(RobotoLightFontItalicSingleton.getInstance(getContext()).getTypeface());
                    break;
            }
        }
    }
}
