package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import org.gdgmumbai.utils.RobotoFontBoldItalicSingleton;
import org.gdgmumbai.utils.RobotoFontBoldSingleton;
import org.gdgmumbai.utils.RobotoFontItalicSingleton;
import org.gdgmumbai.utils.RobotoRegularSingleton;

/**
 * Created by Ammar Githam on 11-09-2014.
 */
public class RobotRegularTextView extends AppCompatTextView {

    public RobotRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotRegularTextView(Context context) {
        super(context);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {

        if (!isInEditMode()) {
            switch (style) {

                case Typeface.NORMAL:
                    setTypeface(RobotoRegularSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.BOLD:
                    setTypeface(RobotoFontBoldSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.ITALIC:
                    setTypeface(RobotoFontItalicSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.BOLD_ITALIC:
                    setTypeface(RobotoFontBoldItalicSingleton.getInstance(getContext()).getTypeface());
                    break;

            }
        }
    }
}
