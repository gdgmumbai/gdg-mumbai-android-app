package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import org.gdgmumbai.utils.RobotoMediumItalicSingleton;
import org.gdgmumbai.utils.RobotoMediumSingleton;

/**
 * Created by Ammar Githam on 11-09-2014.
 */
public class RobotMediumTextView extends AppCompatTextView {

    public RobotMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotMediumTextView(Context context) {
        super(context);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {

        if (!isInEditMode()) {
            switch (style) {

                default:
                case Typeface.NORMAL:
                    setTypeface(RobotoMediumSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.ITALIC:
                    setTypeface(RobotoMediumItalicSingleton.getInstance(getContext()).getTypeface());
                    break;
            }
        }
    }
}
