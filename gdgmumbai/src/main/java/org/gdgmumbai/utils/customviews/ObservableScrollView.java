package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ammar Githam on 02-10-2014.
 */
public class ObservableScrollView extends ScrollView {
    private List<ObservableScrollView.Callbacks> mCallbacks = new ArrayList<>();

    public ObservableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        for (ObservableScrollView.Callbacks c : mCallbacks) {
            c.onScrollChanged(l - oldl, t - oldt);
        }
    }

    @Override
    public int computeVerticalScrollRange() {
        return super.computeVerticalScrollRange();
    }

    public void addCallbacks(ObservableScrollView.Callbacks listener) {
        if (!mCallbacks.contains(listener)) {
            mCallbacks.add(listener);
        }
    }

    public interface Callbacks {
        void onScrollChanged(int deltaX, int deltaY);
    }
}
