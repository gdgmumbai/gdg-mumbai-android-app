package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import org.gdgmumbai.utils.RobotoMediumItalicSingleton;
import org.gdgmumbai.utils.RobotoMediumSingleton;

/**
 * Created by agitham on 23/4/2015.
 */
public class RobotoMediumButton extends AppCompatButton {

    public RobotoMediumButton(Context context) {
        super(context);
    }

    public RobotoMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoMediumButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {

        if (!isInEditMode()) {
            switch (style) {

                default:
                case Typeface.NORMAL:
                    setTypeface(RobotoMediumSingleton.getInstance(getContext()).getTypeface());
                    break;
                case Typeface.ITALIC:
                    setTypeface(RobotoMediumItalicSingleton.getInstance(getContext()).getTypeface());
                    break;
            }
        }
    }
}
