package org.gdgmumbai.utils.customviews;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by Ammar Githam on 02-10-2014.
 */
public class WorkaroundMapFragment extends SupportMapFragment {

    public static WorkaroundMapFragment newInstance() {

        Bundle args = new Bundle();
        WorkaroundMapFragment fragment = new WorkaroundMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstance) {
        View layout = super.onCreateView(layoutInflater, viewGroup, savedInstance);

        TouchableWrapper frameLayout = new TouchableWrapper(getActivity());

        frameLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        if (layout != null)
            ((ViewGroup) layout).addView(frameLayout,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Fragment fragment = getParentFragment();

        if (fragment instanceof OnMapReadyListener) {
            getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    ((OnMapReadyListener) fragment).onMapReady(googleMap);
                }
            });
        }
    }

    public interface OnTouchListener {
        void onTouch();
    }

    /**
     * Listener interface to tell when the map is ready
     */
    public interface OnMapReadyListener {
        void onMapReady(GoogleMap map);
    }

    public class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {

            Fragment fragment = getParentFragment();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (fragment instanceof WorkaroundMapFragment.OnTouchListener) {
                        ((WorkaroundMapFragment.OnTouchListener) fragment).onTouch();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (fragment instanceof WorkaroundMapFragment.OnTouchListener) {
                        ((WorkaroundMapFragment.OnTouchListener) fragment).onTouch();
                    }
                    break;
            }

            return super.dispatchTouchEvent(event);
        }
    }
}
