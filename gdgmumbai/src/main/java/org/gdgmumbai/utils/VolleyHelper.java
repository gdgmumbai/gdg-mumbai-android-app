package org.gdgmumbai.utils;

import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class VolleyHelper {

    private static final int SOCKET_TIMEOUT_MS = 15000;
    private static final int MAX_RETRIES = 3;
    private static final String TAG = "VolleyHelper";

    @Deprecated
    public static void requestJSON(RequestQueue queue, String url, Response.Listener<JSONObject> jsonObjectListener, Response.ErrorListener errorListener) {

        //JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, null, jsonObjectListener, errorListener);
        //queue.add(jsonObjReq);
    }

    public static void requestJSONArray(RequestQueue queue, String url, Response.Listener<JSONArray> jsonArrayListener, Response.ErrorListener errorListener, String tag, boolean forceRefresh) {

        Calendar calendar = Calendar.getInstance();
        Cache.Entry entry = queue.getCache().get(url);

        long serverDate = 0;

        if (entry != null)
            serverDate = entry.serverDate;

        Log.d(TAG, "requestJSONArray url: " + url + ", serverDate: " + new Date(serverDate));

        if (serverDate != 0 && (AppTools.getHoursDifference(serverDate, calendar.getTimeInMillis()) >= 24 || forceRefresh)) {
            queue.getCache().invalidate(url, true);
            Log.d(TAG, "requestJSONArray url: " + url + " invalidated.");
        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, jsonArrayListener, errorListener);
        jsonArrayRequest.setShouldCache(true);
        jsonArrayRequest.setTag(tag);
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonArrayRequest);
    }

    public static void requestJSONObject(RequestQueue queue, String url, Response.Listener<JSONObject> jsonObjectListener, Response.ErrorListener errorListener, String tag, boolean forceRefresh) {

        Calendar calendar = Calendar.getInstance();
        Cache.Entry entry = queue.getCache().get(url);

        long serverDate = 0;

        if (entry != null)
            serverDate = entry.serverDate;

        Log.d(TAG, "requestJSONObject url: " + url + ", serverDate: " + new Date(serverDate));

        if (serverDate != 0 && (AppTools.getHoursDifference(serverDate, calendar.getTimeInMillis()) >= 24 || forceRefresh)) {
            queue.getCache().invalidate(url, true);
            Log.d(TAG, "requestJSONObject url: " + url + " invalidated.");
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonObjectListener, errorListener);
        jsonObjectRequest.setShouldCache(true);
        jsonObjectRequest.setTag(tag);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);
    }
}
