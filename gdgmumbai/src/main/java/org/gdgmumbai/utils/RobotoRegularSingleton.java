package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoRegularSingleton {

    private Context context;
    private static RobotoRegularSingleton instance;
    private Typeface tf;

    private RobotoRegularSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoRegularSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoRegularSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Regular.ttf");
        }

        return tf;
    }
}