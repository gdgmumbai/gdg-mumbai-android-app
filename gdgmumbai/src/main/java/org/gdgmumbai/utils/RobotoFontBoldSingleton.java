package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoFontBoldSingleton {

    private Context context;
    private static RobotoFontBoldSingleton instance;
    private Typeface tf;

    private RobotoFontBoldSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoFontBoldSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoFontBoldSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Bold.ttf");
        }

        return tf;
    }
}