package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoMediumSingleton {

    private static RobotoMediumSingleton instance;
    private Typeface tf;
    private Context context;

    private RobotoMediumSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoMediumSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoMediumSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Medium.ttf");
        }

        return tf;
    }
}