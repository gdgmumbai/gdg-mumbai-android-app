package org.gdgmumbai.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.ColorUtils;
import android.view.View;
import android.view.ViewGroup;

import org.gdgmumbai.R;

/**
 * Created by agitham on 11/9/2014.
 */
public class AppTools {

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        int height = options.outHeight;
        int width = options.outWidth;

        // System.out.println("outHieght: " + options.outHeight + " outWidth: " + options.outWidth);

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            int halfHeight = height / 2;
            int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static void setMargins(View v, int l, int t, int r, int b) {

        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    /*public static void setInsets(Activity context, View view) {

        SystemBarTintManager tintManager = new SystemBarTintManager(context);
        SystemBarTintManager.SystemBarConfig config = tintManager.getConfig();
        view.setPadding(0, config.getPixelInsetTop(false), config.getPixelInsetRight(), 0);
    }*/

    /*public static boolean hasKitkat() {

        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }*/

    public static float getProgress(int value, int min, int max) {

        if (min == max) {
            throw new IllegalArgumentException("Max (" + max + ") cannot equal min (" + min + ")");
        }

        return (value - min) / (float) (max - min);
    }

    public static int getActionBarSize(Context context) {
        if (context == null) {
            return 0;
        }

        float size = context.getResources().getDimensionPixelSize(R.dimen.actionbar_height);

        return (int) size;
    }

    /*public static Bitmap getRandomEventPlaceholder(Resources resources) {

        int[] drawableIds = {
                R.drawable.event_placeholder_female_informal_blue,
                R.drawable.event_placeholder_female_informal_green,
                R.drawable.event_placeholder_female_informal_red,
                R.drawable.event_placeholder_female_informal_yellow,
                R.drawable.event_placeholder_male_formal_blue,
                R.drawable.event_placeholder_male_formal_green,
                R.drawable.event_placeholder_male_formal_red,
                R.drawable.event_placeholder_male_formal_yellow,
                R.drawable.event_placeholder_male_informal_blue,
                R.drawable.event_placeholder_male_informal_green,
                R.drawable.event_placeholder_male_informal_red,
                R.drawable.event_placeholder_male_informal_yellow
        };

        return BitmapFactory.decodeResource(resources, drawableIds[new Random().nextInt(drawableIds.length)]);
    }*/

    public static boolean isNullOrWhitespace(String string) {
        return string == null || string.trim().isEmpty();
    }

    public static int getStatusBarHeight(Resources res) {
        int result = 0;
        int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = res.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getTextColorAlphaForBackground(int backgroundColor, int textColor) {

        float minContrastRatio = 4.5f; // We want a minimum contrast ration of 1:4.5

        int minAlpha = ColorUtils.calculateMinimumAlpha(textColor, backgroundColor, minContrastRatio);

        if (minAlpha != -1) {
            // There is an alpha value which has enough contrast, use it!
            return ColorUtils.setAlphaComponent(textColor, minAlpha);
        }

        return textColor;
    }

    public static long getHoursDifference(long timeStart, long timeStop) {
        long diff = timeStop - timeStart;
        return diff / (60 * 60 * 1000);
    }

    public static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
