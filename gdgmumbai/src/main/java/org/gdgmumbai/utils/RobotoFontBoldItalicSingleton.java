package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoFontBoldItalicSingleton {

    private Context context;
    private static RobotoFontBoldItalicSingleton instance;
    private Typeface tf;

    private RobotoFontBoldItalicSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoFontBoldItalicSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoFontBoldItalicSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-BoldItalic.ttf");
        }

        return tf;
    }
}