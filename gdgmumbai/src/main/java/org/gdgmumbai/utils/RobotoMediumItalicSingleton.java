package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoMediumItalicSingleton {

    private static RobotoMediumItalicSingleton instance;
    private Typeface tf;
    private Context context;

    private RobotoMediumItalicSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoMediumItalicSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoMediumItalicSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-MediumItalic.ttf");
        }

        return tf;
    }
}