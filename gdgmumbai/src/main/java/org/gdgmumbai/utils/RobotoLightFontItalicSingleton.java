package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoLightFontItalicSingleton {

    private Context context;
    private static RobotoLightFontItalicSingleton instance;
    private Typeface tf;

    private RobotoLightFontItalicSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoLightFontItalicSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoLightFontItalicSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-LightItalic.ttf");
        }

        return tf;
    }
}