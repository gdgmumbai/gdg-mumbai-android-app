package org.gdgmumbai.utils.serviceutils;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.gdgmumbai.db.AlarmDbHelper;
import org.gdgmumbai.db.AlarmParam;

import java.util.AbstractList;

/**
 * Created by sukheshaadhikari on 03/10/14.
 */

public class AppBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmHelper ah = new AlarmHelper();
        AlarmDbHelper adm = new AlarmDbHelper(context);

        boolean alarmDown = (PendingIntent.getBroadcast(context, 0, new Intent("org.gdgmumbai.serviceutils"), PendingIntent.FLAG_NO_CREATE) == null);
        if (alarmDown) {
            Toast.makeText(context, "the reminder is assigned ", Toast.LENGTH_LONG).show();
            ah.setNotification(context, "title", "subtitle");
            // TODO to clear the notification dont knw how to get it done.
            AbstractList<AlarmParam> alarms = adm.getAlarms();
            if (!(alarms.isEmpty())) {
                for (int i = 0; i < alarms.size(); i++) {
                    ah.setAlarm(context, i, alarms.get(i).getYear(), alarms.get(i).getMonth(), alarms.get(i).getDay(), alarms.get(i).getHour(), alarms.get(i).getMinute());
                }
            }
        }
    }
}
