package org.gdgmumbai.utils.serviceutils;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import org.gdgmumbai.R;
import org.gdgmumbai.common.MainActivity;

/**
 * Created by sukheshaadhikari on 23/10/14.
 */
class AlarmHelper {

    public void setAlarm(Context ctx, int id, int year, int month, int day, int hour, int minute) {
        Intent intent = new Intent(ctx, AppBroadcast.class);
        PendingIntent pend = PendingIntent.getBroadcast(ctx, id, intent, PendingIntent.FLAG_ONE_SHOT);
        CalendarHelper ch = new CalendarHelper();
        AlarmManager am = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            am.setExact(AlarmManager.RTC_WAKEUP, ch.getCalendar(ctx, year, month, day, hour, minute).getTimeInMillis(), pend);

        Toast.makeText(ctx, "added", Toast.LENGTH_LONG).show();
    }

    public void setNotification(Context ctx, String title, String content) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx)
                .setSubText(title)
                .setSmallIcon(R.drawable.ic_launcher)
                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                .setContentTitle(content);
        Intent resultActivity = new Intent(ctx, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultActivity);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }


}
