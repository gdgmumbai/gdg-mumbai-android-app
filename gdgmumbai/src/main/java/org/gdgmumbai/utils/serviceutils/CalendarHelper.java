package org.gdgmumbai.utils.serviceutils;

import android.content.Context;

import java.util.Calendar;

/**
 * Created by sukheshaadhikari on 03/10/14.
 */
class CalendarHelper {
    private Calendar calendar;

    public Calendar getCalendar(Context context, int year, int month, int day, int hour, int minute) {
        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }
}
