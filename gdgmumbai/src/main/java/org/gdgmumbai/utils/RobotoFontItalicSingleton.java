package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoFontItalicSingleton {

    private Context context;
    private static RobotoFontItalicSingleton instance;
    private Typeface tf;

    private RobotoFontItalicSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoFontItalicSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoFontItalicSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Italic.ttf");
        }

        return tf;
    }
}