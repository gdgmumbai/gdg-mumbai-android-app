package org.gdgmumbai.utils;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoLightFontNormalSingleton {

    private Context context;
    private static RobotoLightFontNormalSingleton instance;
    private Typeface tf;

    private RobotoLightFontNormalSingleton(Context context) {
        this.context = context;
    }

    public static synchronized RobotoLightFontNormalSingleton getInstance(Context context) {
        if (instance == null)
            instance = new RobotoLightFontNormalSingleton(context);
        return instance;
    }

    public Typeface getTypeface() {

        if (tf == null) {
            tf = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Light.ttf");
        }

        return tf;
    }
}