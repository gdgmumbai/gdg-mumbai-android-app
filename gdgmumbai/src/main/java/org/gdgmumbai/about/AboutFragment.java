package org.gdgmumbai.about;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.gdgmumbai.R;
import org.gdgmumbai.common.MainActivity;

public class AboutFragment extends Fragment {

    public static final String TAG = "AboutFragment";

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (getActivity() == null || !(getActivity() instanceof MainActivity))
            return null;

        if (((MainActivity) getActivity()).getToolbar() != null) {
            ((MainActivity) getActivity()).getToolbar().setBackgroundColor(getResources().getColor(R.color.action_bar_green));
        }

        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getResources().getStringArray(R.array.navigation_items_array)[1]);
        }

        DrawerLayout mDrawerLayout = ((MainActivity) getActivity()).getDrawerLayout();
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.about_primary_dark));

        return inflater.inflate(R.layout.fragment_about, container, false);
    }

}