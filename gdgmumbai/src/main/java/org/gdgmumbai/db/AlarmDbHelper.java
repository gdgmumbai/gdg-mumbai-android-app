package org.gdgmumbai.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by sukheshaadhikari on 03/10/14.
 */
public class AlarmDbHelper extends SQLiteOpenHelper {

    private static final String TABLE_NAME = "events";
    private static final String DB_NAME = "GDGRemind";
    private static final String CREATE_TABLE = "Create Table events ( name text,int year, month int , day int ,hour int, minute int ) ";

    public AlarmDbHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public void addAlarmDetails(ContentValues cv) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, cv);
        db.close();
    }

    public void deleteAlarmDetails(String eventName) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, "name = " + eventName, null);
        db.close();
    }

    public void updateAlarmDateDetails(String eventDate) {
        SQLiteDatabase db = getWritableDatabase();
        //TODO Logic to be discussed.
    }

    public ArrayList<AlarmParam> getAlarms() {
        AlarmParam ap;
        ArrayList<AlarmParam> al = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("Select * from " + TABLE_NAME, null);
        while (c.moveToNext()) {
            ap = new AlarmParam();
            ap.setName(c.getString(c.getColumnIndex("name")));
            ap.setYear(c.getInt(c.getColumnIndex("year")));
            ap.setMonth(c.getInt(c.getColumnIndex("month")));
            ap.setDay(c.getInt(c.getColumnIndex("day")));
            ap.setHour(c.getInt(c.getColumnIndex("hour")));
            ap.setMinute(c.getInt(c.getColumnIndex("minute")));
            al.add(ap);
        }
        c.close();
        db.close();
        return al;
    }

    public void setAlarms(ContentValues cv) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, cv);
        db.close();
    }

    public int getCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("Select * from events", null);
        int count = c.getCount();
        c.close();
        db.close();
        return count;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
