package org.gdgmumbai.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.facebook.share.widget.LikeView;
import com.google.android.gms.plus.PlusOneButton;

import org.gdgmumbai.R;
import org.gdgmumbai.about.AboutFragment;
import org.gdgmumbai.contact.ContactFragment;
import org.gdgmumbai.events.EventsFragment;
import org.gdgmumbai.utils.AppTools;

public class MainActivity extends BaseActivity {

    private static final String TAG = "MainActivity";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private int mCurrentSelectedItemId;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected int getThemeResource() {
        return R.style.AppTheme;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        final View navigationHeaderView = mNavigationView.getHeaderView(0);
        Log.d(TAG, "navigationHeaderView: " + navigationHeaderView);
        final View navigationFooterView = mNavigationView.findViewById(R.id.nav_drawer_footer);

        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                //Setting header height according to design specs
                int drawerWidth = mNavigationView.getWidth();
                int headerHeight = (drawerWidth * 9 / 16) + 3;
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) navigationHeaderView.getLayoutParams();
                layoutParams.height = headerHeight;
                navigationHeaderView.setLayoutParams(layoutParams);

                int footerHeight = navigationFooterView.getHeight();
                mNavigationView.setPadding(mNavigationView.getPaddingLeft(), mNavigationView.getPaddingTop(), mNavigationView.getPaddingRight(), footerHeight);

                NavigationView.LayoutParams layoutParams1 = (NavigationView.LayoutParams) navigationFooterView.getLayoutParams();
                layoutParams1.bottomMargin = -footerHeight;
                navigationFooterView.setLayoutParams(layoutParams1);

                mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        setUpNavigation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View shadow = findViewById(R.id.bottom_shadow);
            shadow.setVisibility(View.GONE);
        }

        mCurrentSelectedItemId = savedInstanceState == null ? R.id.events : savedInstanceState.getInt(STATE_SELECTED_POSITION);

        onNavigationDrawerItemSelected(mCurrentSelectedItemId);
    }

    private void setUpNavigation() {

        final Toolbar toolbar = getToolbar();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {

                if (mDrawerLayout != null) {

                    mDrawerLayout.closeDrawer(GravityCompat.START);

                    if (menuItem.getItemId() != mCurrentSelectedItemId)
                        expandToolbar();

                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            onNavigationDrawerItemSelected(menuItem.getItemId());
                        }
                    };
                    h.postDelayed(r, 300);
                }

                return true;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                toolbar,
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                toolbar.setTitle(mNavigationView.getMenu().findItem(mCurrentSelectedItemId).getTitle());
                invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0);
                toolbar.setTitle(R.string.app_name);
                invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (drawerView != null) {
                    super.onDrawerSlide(drawerView, 0);
                }
                else {
                    super.onDrawerSlide(drawerView, slideOffset);
                }
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        View contributorLink = mNavigationView.findViewById(R.id.contributor_form_nav_item);
        contributorLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/forms/d/1R7LGy7qyL-ork0sQfIeWCeMAO5N4zLQpmlNc4WPpj_Y/viewform"));
                startActivity(browserIntent);
            }
        });

        LikeView likeView = (LikeView) mNavigationView.findViewById(R.id.likeView);
        likeView.setLikeViewStyle(LikeView.Style.BUTTON);
        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);
        likeView.setObjectIdAndType("http://www.facebook.com/GoogleDeveloperGroupMumbai", LikeView.ObjectType.PAGE);

        if (!AppTools.isPackageInstalled("com.facebook.katana", this))
            likeView.setEnabled(false);

        PlusOneButton mPlusOneButton = (PlusOneButton) mNavigationView.findViewById(R.id.plus_one_button);
        mPlusOneButton.initialize("http://plus.google.com/u/0/+GDGMumbaiOrg/", 1);

        View openSourceLicensesItemView = mNavigationView.findViewById(R.id.open_source_license_nav_item);
        openSourceLicensesItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LicensesDialogFragment.displayLicensesFragment(getSupportFragmentManager(), false);
                toggleDrawerState();
            }
        });
    }

    private void onNavigationDrawerItemSelected(int menuItemId) {

        mNavigationView.getMenu().findItem(menuItemId).setChecked(true);
        mCurrentSelectedItemId = menuItemId;
        Fragment fragment = null;
        String tag = "";
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (menuItemId) {
            case R.id.events:
                fragment = fragmentManager.findFragmentByTag(EventsFragment.TAG);
                tag = EventsFragment.TAG;

                if (fragment == null) {
                    fragment = EventsFragment.newInstance();
                }
                break;

            case R.id.about:
                fragment = AboutFragment.newInstance();
                tag = AboutFragment.TAG;
                break;

            case R.id.contact:
                fragment = ContactFragment.newInstance();
                tag = ContactFragment.TAG;
                break;
        }

        if (fragment != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment, tag)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        /*if (!isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.my, menu);
            return true;
        }*/
        return super.onCreateOptionsMenu(menu);
    }

    private boolean isDrawerOpen() {

        return mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (isDrawerOpen()) {
            toggleDrawerState();
            return;
        }

        super.onBackPressed();
    }

    private void toggleDrawerState() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedItemId);
    }

    public void expandToolbar() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((AppBarLayout) getToolbar().getParent()).getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        if (behavior != null) {
            behavior.setTopAndBottomOffset(0);
            behavior.onNestedPreScroll((CoordinatorLayout) (getToolbar().getParent()).getParent(), ((AppBarLayout) getToolbar().getParent()), null, 0, 1, new int[2]);
        }
    }
}
