package org.gdgmumbai.common;

import android.content.Intent;
import android.os.Handler;

import org.gdgmumbai.R;

/**
 * Created by agitham on 15/7/2015.
 */
public class SplashScreenActivity extends BaseActivity {
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splashscreen;
    }

    @Override
    protected int getThemeResource() {
        return R.style.AppTheme;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Intent mainActivityIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(mainActivityIntent);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        };
        handler.postDelayed(runnable, 600);
    }
}
