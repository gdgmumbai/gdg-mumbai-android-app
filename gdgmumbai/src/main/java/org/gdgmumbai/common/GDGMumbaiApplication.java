package org.gdgmumbai.common;

import android.app.Application;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import org.gdgmumbai.R;

/**
 * Created by agitham on 20/7/2015.
 */
public class GDGMumbaiApplication extends Application {

    private static final String TAG = "GDGMumbaiApplication";
    private static GoogleAnalytics analytics;

    @Override
    public void onCreate() {
        super.onCreate();
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);
        analytics.enableAutoActivityReports(this);
        analytics.setDryRun(getResources().getBoolean(R.bool.analytics_dry_run_enabled));
        getTracker();

        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public Tracker getTracker() {

        try {
            Tracker tracker = analytics.newTracker("UA-65334611-1");
            tracker.enableExceptionReporting(getResources().getBoolean(R.bool.analytics_exception_reporting_enabled));
            tracker.enableAdvertisingIdCollection(true);
            tracker.enableAutoActivityTracking(true);
            return tracker;
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to initialize Google Analytics V4");
        }

        return null;
    }
}
