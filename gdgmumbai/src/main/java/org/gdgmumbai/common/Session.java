package org.gdgmumbai.common;

/**
 * Created by Ammar Githam on 19-10-2014.
 */
public class Session {

    private int headerId;
    private String header;
    private String time;
    private String topic;
    private String description;
    private Speaker speaker;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getHeaderId() {
        return headerId;
    }

    public void setHeaderId(int headerId) {
        this.headerId = headerId;
    }

    @Override
    public String toString() {
        return "Session{" +
                "headerId=" + headerId +
                ", header='" + header + '\'' +
                ", time='" + time + '\'' +
                ", topic='" + topic + '\'' +
                ", description='" + description + '\'' +
                ", speaker=" + speaker +
                '}';
    }
}
