package org.gdgmumbai.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.gdgmumbai.R;


/**
 * Created by Ammar Githam on 18-10-2014.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";
    private Toolbar actionBar;

    public Toolbar getToolbar() {

        if (actionBar == null) {
            actionBar = (Toolbar) findViewById(R.id.actionbar);
        }

        return actionBar;
    }

    protected abstract int getLayoutResource();

    protected abstract int getThemeResource();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(getThemeResource());
        setContentView(getLayoutResource());
        actionBar = getToolbar();

        if (actionBar != null) {
            setSupportActionBar(actionBar);
        }
    }
}