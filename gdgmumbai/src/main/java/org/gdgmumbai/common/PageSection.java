package org.gdgmumbai.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ammar Githam on 29-03-2015.
 */
public class PageSection {

    private String heading;
    private String subHeading;
    private String content;
    private ArrayList<ButtonLink> buttonLinks;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<ButtonLink> getButtonLinks() {
        return buttonLinks;
    }

    public void setButtonLinks(ArrayList<ButtonLink> buttonLinks) {
        this.buttonLinks = buttonLinks;
    }
}
