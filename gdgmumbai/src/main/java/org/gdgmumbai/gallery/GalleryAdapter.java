package org.gdgmumbai.gallery;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.gdgmumbai.R;
import org.gdgmumbai.utils.AppTools;

import java.util.List;

public class GalleryAdapter extends BaseAdapter {

    private static final String TAG = "GalleryAdapter";
    private Context context;
    private LayoutInflater li;
    private List<GalleryItem> galleryItems;

    public GalleryAdapter(Context context, List<GalleryItem> galleryItems) {

        this.context = context;
        this.galleryItems = galleryItems;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return galleryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return galleryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GalleryItemViewHolder galleryItemViewHolder;

        if (convertView == null) {
            convertView = li.inflate(R.layout.gallery_item, parent, false);
            galleryItemViewHolder = new GalleryItemViewHolder();
            galleryItemViewHolder.name = (TextView) convertView.findViewById(R.id.gallery_text);
            galleryItemViewHolder.hero = (ImageView) convertView.findViewById(R.id.gallery_image);
            convertView.setTag(galleryItemViewHolder);
        }
        else {
            galleryItemViewHolder = (GalleryItemViewHolder) convertView.getTag();
        }

        int height = galleryItemViewHolder.hero.getMeasuredHeight();
        int width = galleryItemViewHolder.hero.getMeasuredWidth();

        //Log.d(TAG, "Before height: " + height + " width: " + width);

        galleryItemViewHolder.name.setTypeface(null, Typeface.NORMAL);
        galleryItemViewHolder.name.setText(galleryItems.get(position).getName());

        if (height == 0 || width == 0) {

            final ImageView heroImageView = galleryItemViewHolder.hero;
            final int imageViewPosition = galleryItems.get(position).getImageResId();

            ViewTreeObserver viewTreeObserver = heroImageView.getViewTreeObserver();

            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        try {
                            int finalHeight = heroImageView.getMeasuredHeight();
                            int finalWidth = heroImageView.getMeasuredWidth();

                            heroImageView.setImageBitmap(AppTools.decodeSampledBitmapFromResource(context.getResources(), imageViewPosition, finalWidth, finalHeight));
                            heroImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        catch (NoSuchMethodError x) {
                            heroImageView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
                });
            }
        }
        else
            galleryItemViewHolder.hero.setImageBitmap(AppTools.decodeSampledBitmapFromResource(context.getResources(), galleryItems.get(position).getImageResId(), width, height));

        return convertView;
    }

    private static class GalleryItemViewHolder {

        TextView name;
        ImageView hero;
    }
}
