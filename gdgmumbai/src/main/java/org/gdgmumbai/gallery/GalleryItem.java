package org.gdgmumbai.gallery;

/**
 * Created by sukheshaadhikari on 03/09/14.
 */
public class GalleryItem
{
    int imageResId;
    String name;

    public int getImageResId()
    {
        return imageResId;
    }

    public void setImageResId(int imageResId)
    {
        this.imageResId = imageResId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
