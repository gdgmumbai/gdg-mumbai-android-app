package org.gdgmumbai.gallery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.gdgmumbai.R;
import org.gdgmumbai.common.MainActivity;
import org.json.JSONObject;

import java.util.ArrayList;

public class GalleryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ArrayList<GalleryItem> galleryItems = new ArrayList<>();
    private SwipeRefreshLayout swp;

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainActivity) getActivity()).getToolbar().setBackgroundColor(getResources().getColor(R.color.action_bar_red));
        /*SystemBarTintManager systemBarTintManager = new SystemBarTintManager(getActivity());
        systemBarTintManager.setStatusBarTintEnabled(true);
        systemBarTintManager.setStatusBarTintResource(R.color.action_bar_red);

        GalleryItem galleryItem;
        {
            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img1);
            galleryItem.setName("ABC1");
            galleryItems.add(galleryItem);

            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img2);
            galleryItem.setName("ABC2");
            galleryItems.add(galleryItem);

            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img3);
            galleryItem.setName("ABC3");
            galleryItems.add(galleryItem);

            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img4);
            galleryItem.setName("ABC4");
            galleryItems.add(galleryItem);

            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img5);
            galleryItem.setName("ABC5");
            galleryItems.add(galleryItem);

            galleryItem = new GalleryItem();
            galleryItem.setImageResId(R.drawable.img6);
            galleryItem.setName("ABC6");
            galleryItems.add(galleryItem);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.img_gal);
        swp = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        swp.setOnRefreshListener(this);
        swp.setColorScheme(R.color.events_primary, R.color.red, R.color.events_primary, R.color.red);
        swp.setRefreshing(true);
        retrievingData();
        GalleryAdapter gridAdapter = new GalleryAdapter(getActivity(), galleryItems);
        gridView.setAdapter(gridAdapter);

        return view;
    }

    @Override
    public void onRefresh() {
        retrievingData();
    }

    public void setData(JSONObject o) {
        swp.setRefreshing(false);
    }

    public void retrievingData() {
        swp.setRefreshing(false);
    }
}
