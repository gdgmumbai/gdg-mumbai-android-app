package org.gdgmumbai.contact;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.gdgmumbai.R;
import org.gdgmumbai.common.MainActivity;

public class ContactFragment extends Fragment {

    public static final String TAG = "ContactFragment";

    public static ContactFragment newInstance() {
        return new ContactFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getActivity() == null || !(getActivity() instanceof MainActivity))
            return null;

        if (((MainActivity) getActivity()).getToolbar() != null) {
            ((MainActivity) getActivity()).getToolbar().setBackgroundColor(getResources().getColor(R.color.action_bar_yellow));
        }

        if (((MainActivity)getActivity()).getSupportActionBar() != null)
        {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getResources().getStringArray(R.array.navigation_items_array)[2]);
        }
        
        DrawerLayout mDrawerLayout = ((MainActivity) getActivity()).getDrawerLayout();
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.contact_primary_dark));

        return inflater.inflate(R.layout.fragment_contact, container, false);
    }
}