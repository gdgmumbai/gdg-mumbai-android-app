package org.gdgmumbai.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.gdgmumbai.events.Event;
import org.gdgmumbai.events.EventsCache;
import org.gdgmumbai.utils.VolleyHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ammar Githam on 19-10-2014.
 */
public class ApiHelperImpl implements ApiHelper {

    private static final String TAG = "ApiHelperImpl";

    @Override
    public void requestAllEvents(final Context context, RequestQueue requestQueue, EventsRequestCallback eventsRequestCallback, String tag, boolean forceRefresh) {

        // TODO Pagination
        final EventsRequestCallback finalEventsRequestCallback = eventsRequestCallback;

        Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {

                if (context != null) {
                    //TODO Saving only the first page json.
                    SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(EventsCache.KEY_CACHED_EVENTS_JSON, jsonArray.toString());
                    editor.apply();
                }

                ArrayList<Event> events = JSONParser.parseEvents(jsonArray);
                finalEventsRequestCallback.onEventsReceived(events);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, "onErrorResponse volleyError: " + volleyError.getLocalizedMessage());
                finalEventsRequestCallback.onErrorResponse(volleyError);
            }
        };

        VolleyHelper.requestJSONArray(requestQueue, ApiConstants.EVENTS_URL, jsonArrayListener, errorListener, tag, forceRefresh);
    }

    @Override
    public void requestEvent(int id, RequestQueue requestQueue, EventRequestCallback eventRequestCallback, String tag, boolean forceRefresh) {
        // TODO Pagination
        final EventRequestCallback finalEventRequestCallback = eventRequestCallback;

        Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {

                Event event = JSONParser.parseEvent(jsonArray);
                finalEventRequestCallback.onEventReceived(event);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse volleyError: " + volleyError);
                finalEventRequestCallback.onErrorResponse(volleyError);
            }
        };


        VolleyHelper.requestJSONArray(requestQueue, ApiConstants.EVENT_ID_URL + id, jsonArrayListener, errorListener, tag, forceRefresh);
    }

    public void requestImageUrls(final Context context, final String id, RequestQueue requestQueue, ImageRequestCallback imageRequestCallback, final int eventId, boolean forceRefresh) {
        final ImageRequestCallback finalImageRequestCallback = imageRequestCallback;

        Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                if (context != null) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(EventsCache.KEY_CACHED_IMAGES + id, jsonObject.toString());
                    editor.apply();
                }

                String[] urls = new String[2];
                try {
                    if (jsonObject.has("is_image") && jsonObject.getBoolean("is_image")) {
                        JSONObject sizes = jsonObject.getJSONObject("attachment_meta").getJSONObject("sizes");
                        String thumbnailUrl = sizes.getJSONObject("thumbnail").getString("url");
                        String largeUrl = sizes.getJSONObject("large").getString("url");
                        urls[0] = thumbnailUrl;
                        urls[1] = largeUrl;
                    }
                }
                catch (JSONException e) {
                    Log.e(TAG, "Error parsing urls", e);
                }
                Log.d(TAG, "jsonObject: " + jsonObject.toString());
                finalImageRequestCallback.onImageUrlsReceived(eventId, urls);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, "onErrorResponse volleyError: " + volleyError.getLocalizedMessage());
                finalImageRequestCallback.onErrorResponse(volleyError);
            }
        };

        VolleyHelper.requestJSONObject(requestQueue, ApiConstants.IMAGES_URL + id, jsonObjectListener, errorListener, String.valueOf(eventId), forceRefresh);
    }

    public interface EventsRequestCallback {

        void onEventsReceived(ArrayList<Event> events);

        void onErrorResponse(VolleyError volleyError);
    }

    public interface EventRequestCallback {

        void onEventReceived(Event event);

        void onErrorResponse(VolleyError volleyError);
    }

    public interface ImageRequestCallback {

        void onImageUrlsReceived(int id, String[] urls);

        void onErrorResponse(VolleyError volleyError);
    }
}
