package org.gdgmumbai.api;

import android.text.TextUtils;
import android.util.Log;

import org.gdgmumbai.common.ButtonLink;
import org.gdgmumbai.common.Faq;
import org.gdgmumbai.common.PageSection;
import org.gdgmumbai.common.Partner;
import org.gdgmumbai.common.Session;
import org.gdgmumbai.common.Speaker;
import org.gdgmumbai.events.Event;
import org.gdgmumbai.events.EventAgendaDay;
import org.gdgmumbai.utils.AppTools;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ammar Githam on 28-03-2015.
 */
public class EventHtmlParser {

    private static final String TAG = "EventHtmlParser";
    private static final Iterable<String> ignoreIds = new ArrayList<>(Arrays.asList(
            new String[]{
                    "home",
                    "gallery",
                    "schedule",
                    "speakers",
                    "registernow",
                    "sponsors",
                    "mentors",
                    "register",
                    "contact",
                    "location"
            }
    ));

    private static final Pattern COMPILE = Pattern.compile("\\n", Pattern.LITERAL);

    private static final Pattern BG_IMAGE_PATTERN = Pattern.compile("bg_image_custom=”[\\d]*″");
    private static final Pattern IMAGE_ID_PATTERN = Pattern.compile("[\\d]*");

    public static String getHeaderImageId(String htmlContent) throws Exception {
        try {
            Document doc = Jsoup.parse(htmlContent);
            Element firstP = doc.getElementsByTag("p").first();
            String firstPHtml = firstP.html();
            Matcher m = BG_IMAGE_PATTERN.matcher(firstPHtml);
            while (m.find()) {
                String s = m.group(0);
                Matcher imageIdMatcher = IMAGE_ID_PATTERN.matcher(s);
                Log.d(TAG, "s: " + s);
                while (imageIdMatcher.find()) {
                    String imageId = imageIdMatcher.group(0);
                    if (!TextUtils.isEmpty(imageId)) {
                        return imageId;
                    }
                }
            }
        }
        catch (NullPointerException e) {
            Log.e(TAG, "Error in parsing Header Image URL.", e);
        }
        return null;
    }

    public static String getVenue(String htmlContent) throws Exception {

        String venue = "";
        Document doc = Jsoup.parse(htmlContent);
        Elements mediaHeadingElements = doc.select("h4.media-heading");

        for (int i = 0; i < mediaHeadingElements.size(); i++) {

            Element mediaHeadingElement = mediaHeadingElements.get(i);

            if ("Location".equalsIgnoreCase(mediaHeadingElement.html())) {
                venue = mediaHeadingElement.nextElementSibling().html();
                break;
            }
        }

        //Log.d(TAG, "Venue: " + venue);
        return venue;
    }

    public static double[] getVenueCoordinates(String htmlContent) throws Exception {

        //Log.d(TAG, "htmlContent: " + htmlContent);
        double latitude;
        double longitude;

        // eg. google.maps.LatLng(19.0223, 72.8562)
        String searchString = "google.maps.LatLng(";
        int searchStringIndex = htmlContent.indexOf(searchString);
        String latLongString = htmlContent.substring(searchStringIndex + searchString.length(), htmlContent.indexOf(')', searchStringIndex)); // 19.0223, 72.8562

        String[] latLongParts = latLongString.split(",");
        String latitudeString = latLongParts[0].trim();
        String longitudeString = latLongParts[1].trim();

        latitude = Double.parseDouble(latitudeString);
        longitude = Double.parseDouble(longitudeString);
        //Log.d(TAG, "lat long : " + latitude + " " + longitude);

        return new double[]{latitude, longitude};
    }

    @Deprecated
    public static String[] getDate(String htmlContent) throws Exception {

        String dateString = "";
        Document doc = Jsoup.parse(htmlContent);
        Elements mediaHeadingElements = doc.select("h4.media-heading");

        for (int i = 0; i < mediaHeadingElements.size(); i++) {

            Element mediaHeadingElement = mediaHeadingElements.get(i);

            if ("Date".equalsIgnoreCase(mediaHeadingElement.html())) {
                dateString = mediaHeadingElement.nextElementSibling().html();
                break;
            }
        }

        Log.d(TAG, "Date: " + dateString);

        String startDate;
        String endDate;

        // Currently only supported for date formats are:
        // 1. April 1, 2015
        // 2. March 14 - 15, 2015
        // 3. March 15 - April 4, 2015
        //
        // Anything else will taken as a normal string.

        String regex1 = "\\w+ \\d{1,2}, \\d{4}";
        String regex2 = "\\w+ \\d{1,2} - \\d{1,2}, \\d{4}";
        String regex3 = "\\w+ \\d{1,2} - \\w+ \\d{1,2}, \\d{4}";

        String defaultEventDatePattern = Event.DEFAULT_EVENT_DATE_PATTERN;
        SimpleDateFormat defaultEventSimpleDateFormat = new SimpleDateFormat(defaultEventDatePattern, Locale.getDefault());

        if (dateString.matches(regex1)) {

            // Single Day Event.
            // Converting the format to default event format
            String regExpDatePattern = "MMM dd, yyyy";
            SimpleDateFormat regExpSimpleDateFormat = new SimpleDateFormat(regExpDatePattern, Locale.getDefault());
            startDate = defaultEventSimpleDateFormat.format(regExpSimpleDateFormat.parse(dateString)); // From reg exp pattern to the event pattern.
            endDate = startDate; // Since it is a single day event.
        }
        else if (dateString.matches(regex2)) {

            // Multi day event
            String[] dateParts = dateString.split(",");
            String dates = dateParts[0].trim(); // March 14 - 15
            String year = dateParts[1].trim(); // 2015

            String[] datesArray = dates.split("-");
            String start = datesArray[0].trim(); // March 14
            String endDay = datesArray[1].trim(); // 15

            String[] startDateParts = start.split(" ");
            String month = startDateParts[0].trim(); // March
            String startDay = startDateParts[1].trim(); // 14

            String regExpStartDay = month + " " + startDay + ", " + year; // March 14, 2015
            String regExpEndDay = month + " " + endDay + ", " + year; // March 15, 2015

            String regExpDatePattern = "MMM dd, yyyy";
            SimpleDateFormat regExpSimpleDateFormat = new SimpleDateFormat(regExpDatePattern, Locale.getDefault());

            startDate = defaultEventSimpleDateFormat.format(regExpSimpleDateFormat.parse(regExpStartDay)); // From reg exp pattern to the event pattern.
            endDate = defaultEventSimpleDateFormat.format(regExpSimpleDateFormat.parse(regExpEndDay)); // From reg exp pattern to the event pattern.
        }
        else if (dateString.matches(regex3)) {

            // Multi month event
            String[] dateParts = dateString.split(",");
            String dates = dateParts[0].trim(); // March 15 - April 4
            String year = dateParts[1].trim(); // 2015

            String[] datesArray = dates.split("-");
            String start = datesArray[0].trim(); // March 15
            String end = datesArray[1].trim(); // April 4

            String[] startDateParts = start.split(" ");
            String startMonth = startDateParts[0].trim(); // March
            String startDay = startDateParts[1].trim(); // 15

            String[] endDateParts = end.split(" ");
            String endMonth = endDateParts[0].trim(); // April
            String endDay = endDateParts[1].trim(); // 4

            String regExpStartDay = startMonth + " " + startDay + ", " + year; // March 15, 2015
            String regExpEndDay = endMonth + " " + endDay + ", " + year; // April 4, 2015

            String regExpDatePattern = "MMM dd, yyyy";
            SimpleDateFormat regExpSimpleDateFormat = new SimpleDateFormat(regExpDatePattern, Locale.getDefault());

            startDate = defaultEventSimpleDateFormat.format(regExpSimpleDateFormat.parse(regExpStartDay)); // From reg exp pattern to the event pattern.
            endDate = defaultEventSimpleDateFormat.format(regExpSimpleDateFormat.parse(regExpEndDay)); // From reg exp pattern to the event pattern.

        }
        else {

            // dateString is not matching any supported formats. We will use it as it is.
            startDate = endDate = dateString;
        }


        return new String[]{startDate, endDate};
    }

    public static ArrayList<PageSection> getPageSections(String htmlContent) {

        ArrayList<PageSection> pageSections = new ArrayList<>();

        Document document = Jsoup.parse(htmlContent);

        Elements sectionElements = document.select("section.page-section");

        for (int i = 0; i < sectionElements.size(); i++) {

            Element sectionElement = sectionElements.get(i);

            if (!checkIgnore(sectionElement)) {
                PageSection pageSection = getPageSection(sectionElement);
                pageSections.add(pageSection);
            }
        }

        return pageSections;
    }

    private static PageSection getPageSection(String sectionHtmlString) {

        Document sectionDocument = Jsoup.parse(sectionHtmlString);
        return getPageSection(sectionDocument);
    }

    private static PageSection getPageSection(Element sectionElement) {

        PageSection pageSection = new PageSection();

        try {
            Element titleElement = sectionElement.select("h1.section-title").first().select("span.title-inner").first();
            titleElement.select("small").first().remove();
            pageSection.setHeading(titleElement.text());
            //Log.d(TAG, pageSection.getHeading());
        }
        catch (NullPointerException e) {
            Log.d(TAG, "No heading found for page section.");
        }

        try {
            Element textColumnElement = sectionElement.select("div.wpb_text_column > div.wpb_wrapper").first();
            pageSection.setContent(br2nl(textColumnElement.html()));
        }
        catch (NullPointerException e) {
            Log.d(TAG, "No content found for this page section.");
        }

        Elements linkElements = sectionElement.select("a");

        ArrayList<ButtonLink> buttonLinks = new ArrayList<>();

        for (int i = 0; i < linkElements.size(); i++) {

            Element linkElement = linkElements.get(i);
            ButtonLink buttonLink = new ButtonLink(linkElement.text(), linkElement.attr("href"));
            buttonLinks.add(buttonLink);
        }

        pageSection.setButtonLinks(buttonLinks);

        return pageSection;
    }

    private static boolean checkIgnore(Element sectionElement) {

        if (sectionElement != null) {

            for (String ignoreId : ignoreIds) {
                if (ignoreId.equalsIgnoreCase(sectionElement.id()))
                    return true;
            }
        }
        else {
            return true;
        }

        return false;
    }

    public static ArrayList<Speaker> getSpeakers(String htmlContent) {

        ArrayList<Speaker> speakers = new ArrayList<>();

        Document doc = Jsoup.parse(htmlContent);
        Elements speakersElements = doc.select(".speaker-row");

        for (int i = 0; i < speakersElements.size(); i++) {

            Element speakerElement = speakersElements.get(i);
            int id = Integer.parseInt(speakerElement.id().split("_")[1]);
            String imageUrl = speakerElement.select(".media > img").first().attr("src");
            String name = speakerElement.select("h3.caption-title").first().text();
            String description = speakerElement.select("p.caption-category").first().text();

            Speaker speaker = new Speaker();
            speaker.setId(id);
            speaker.setName(name);
            speaker.setImageURL(imageUrl);
            speaker.setDescription(description);
            speakers.add(speaker);
        }

        return speakers;
    }

    public static ArrayList<Partner> getPartners(String htmlContent) {

        ArrayList<Partner> partners = new ArrayList<>();

        Document doc = Jsoup.parse(htmlContent);
        Elements partnerElements = doc.select("img.partner");

        for (int i = 0; i < partnerElements.size(); i++) {

            Element partnerElement = partnerElements.get(i);
            String imageUrl = partnerElement.attr("src");
            String name = partnerElement.attr("alt");

            Partner partner = new Partner();
            partner.setImageUrl(imageUrl);
            partner.setName(name);
            partners.add(partner);
        }

        return partners;
    }

    public static String getAgendaContent(String htmlContent) {

        String agenda = "";
        Document doc = Jsoup.parse(htmlContent);
        Element scheduleElement = doc.select(".schedule-wrapper").first();
        if (scheduleElement != null) {
            agenda = scheduleElement.html();
        }
        return agenda;

    }

    public static ArrayList<EventAgendaDay> getAgendaDays(String htmlContent) {

        ArrayList<EventAgendaDay> eventAgendaDays = new ArrayList<>();

        Document doc = Jsoup.parse(htmlContent);
        Elements tabs = doc.select(".schedule-tabs").first().select("li");

        for (int i = 0; i < tabs.size(); i++) {

            Element tab = tabs.get(i);

            String title = "";
            try {
                title = tab.select("strong").first().text();
            }
            catch (Exception e) {
                Log.d(TAG, "Title not found for this Tab.");
            }

            String subtitle = "";
            try {
                tab.select("strong").first().remove();
                subtitle = tab.text();
            }
            catch (Exception e) {
                Log.d(TAG, "Subtitle not found for this Tab.");
            }

            String contentId = "";
            try {
                contentId = tab.select("a").first().attr("href").substring(1);
            }
            catch (Exception e) {
                Log.d(TAG, "Content id not found for this Tab.");
            }

            String sessionsContent = "";

            if (!AppTools.isNullOrWhitespace(contentId))
                sessionsContent = EventHtmlParser.getSessionsContentForDay(contentId, doc);

            EventAgendaDay eventAgendaDay = new EventAgendaDay();
            eventAgendaDay.setTitle(title);
            eventAgendaDay.setSubtitle(subtitle);
            eventAgendaDay.setSessionsContent(sessionsContent);

            eventAgendaDays.add(eventAgendaDay);
        }

        return eventAgendaDays;
    }

    private static String getSessionsContentForDay(String contentId, Document doc) {
        Element contentTabsElement = doc.select(".tab-content").first().select("#" + contentId).first();
        return contentTabsElement.html();
    }

    public static ArrayList<Session> getSessionsFromContent(String htmlContent) {

        ArrayList<Session> sessions = new ArrayList<>();

        Document doc = Jsoup.parse(htmlContent);
        Elements contentTabs = doc.select(".schedule-tabs").first().select("li");

        for (int i = 0; i < contentTabs.size(); i++) {

            Element contentTab = contentTabs.get(i);

            String header = "";
            try {
                header = contentTab.select("a").first().text();
            }
            catch (Exception e) {
                Log.d(TAG, "Header Not found");
            }

            String headerContentId = "";
            try {
                headerContentId = contentTab.select("a").first().attr("href").substring(1);
            }
            catch (Exception e) {
                Log.d(TAG, "Header Content id not found");
            }

            if (!AppTools.isNullOrWhitespace(headerContentId)) {

                Element timelineElement = doc.select("div.tab-content").first().select("div.tab-content").first().select("#" + headerContentId).first().select(".timeline").first();
                Elements sessionElements = timelineElement.select("article");

                for (int j = 0; j < sessionElements.size(); j++) {

                    Element sessionElement = sessionElements.get(j);
                    String speakerName = sessionElement.select("div.post-media").first().select("img").first().attr("alt");
                    String speakerImageUrl = sessionElement.select("div.post-media").first().select("img").first().attr("src");
                    String timeText = sessionElement.select("div.post-header").first().select("div.post-meta").first().select("span").first().text();
                    String topic = sessionElement.select("div.post-header").first().select("h2").first().text();
                    String description = sessionElement.select("div.post-body").first().select("div.post-excerpt").first().text();

                    Session session = new Session();
                    session.setTime(timeText.trim());
                    session.setTopic(topic.trim());
                    session.setDescription(description.trim());

                    Speaker speaker = new Speaker();
                    speaker.setName(speakerName.trim());
                    speaker.setImageURL(speakerImageUrl.trim());

                    session.setSpeaker(speaker);
                    session.setHeader(header.trim());
                    session.setHeaderId(i);

                    sessions.add(session);
                }
            }
        }

        return sessions;
    }

    private static String br2nl(String html) {

        Document document = Jsoup.parse(html);
        document.select("br").append("\\n");
        document.select("p").prepend("\\n\\n");
        return COMPILE.matcher(document.text()).replaceAll("\n").trim();
    }

    public static ArrayList<Faq> getFaqs(String htmlContent) {

        ArrayList<Faq> faqs = new ArrayList<>();

        Document doc = Jsoup.parse(htmlContent);
        Element faqContainerElement = doc.select("div.faq").isEmpty() ? null : doc.select("div.faq").first();

        if (faqContainerElement != null) {

            Elements questionElements = faqContainerElement.select("#tabs-faq > li");

            for (Element questionElement : questionElements) {

                String question = questionElement.text();
                String answerId = questionElement.select("a").first().attr("href"); // Will contain # . eg: #tab37
                String answer = faqContainerElement.select(answerId).first().text();

                Faq faq = new Faq();
                faq.setQuestion(question);
                faq.setAnswer(answer);
                faqs.add(faq);
            }
        }

        return faqs;
    }

    public static String getRegistrationURL(String htmlContent) {

        //id can either be registernow or register.
        String link = "";

        Document doc = Jsoup.parse(htmlContent);
        Elements registrationSectionElements = doc.select("#registernow");

        if (registrationSectionElements.isEmpty()) {
            registrationSectionElements = doc.select("#register");
        }

        if (!registrationSectionElements.isEmpty()) {

            Element registrationSectionElement = registrationSectionElements.first();
            Elements anchors = registrationSectionElement.select("a");

            if (!anchors.isEmpty()) {
                link = anchors.first().attr("href");
            }
        }

        return link;
    }
}
