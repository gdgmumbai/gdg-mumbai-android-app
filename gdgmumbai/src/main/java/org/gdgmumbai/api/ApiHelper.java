package org.gdgmumbai.api;

import android.content.Context;

import com.android.volley.RequestQueue;

/**
 * Created by Ammar Githam on 19-10-2014.
 */
public interface ApiHelper {

    /**
     * @param context Context of the request
     * @param requestQueue RequestQueue for the current request
     * @param eventsRequestCallback ApiHelper.EventRequestCallback listener for request success or error
     * @param tag Tag to identify the request
     * @param forceRefresh set true to invalidate cache and force fresh response from server
     */
    void requestAllEvents(Context context, RequestQueue requestQueue, ApiHelperImpl.EventsRequestCallback eventsRequestCallback, String tag, boolean forceRefresh);

    void requestEvent(int id, RequestQueue requestQueue, ApiHelperImpl.EventRequestCallback eventsRequestCallback, String tag, boolean forceRefresh);

}
