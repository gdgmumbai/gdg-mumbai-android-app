package org.gdgmumbai.api;

import android.util.Log;

import org.apache.commons.lang3.StringEscapeUtils;
import org.gdgmumbai.events.Event;
import org.gdgmumbai.utils.AppTools;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ammar Githam on 19-10-2014.
 */
public class JSONParser {

    private static final String TAG = "JSONParser";

    public static ArrayList<Event> parseEvents(JSONArray eventsJsonArray) {

        ArrayList<Event> events = new ArrayList<>();

        try {
            for (int i = 0; i < eventsJsonArray.length(); i++) {

                JSONObject eventJsonObject = eventsJsonArray.getJSONObject(i);
                String content = "";
                if (eventJsonObject.has("content"))
                    content = eventJsonObject.getString("content");

                if (!AppTools.isNullOrWhitespace(content)) {

                    Event event = new Event();
                    event.setEventDetails(eventJsonObject.toString());

                    event.setId(eventJsonObject.getInt(ApiConstants.EVENT_ID));
                    //Log.d(TAG, "ID: " + event.getId());
                    event.setTitle(StringEscapeUtils.unescapeHtml4(eventJsonObject.getString(ApiConstants.EVENT_TITLE)));

                    try {
                        event.setImageId(EventHtmlParser.getHeaderImageId(content));
                        Log.d(TAG, "Image id: " + event.getImageId());
                    }
                    catch (Exception e) {
                        Log.e(TAG, "Error parsing image id", e);
                    }

                    try {
                        event.setVenue(StringEscapeUtils.unescapeHtml4(EventHtmlParser.getVenue(content)));
                    }
                    catch (Exception e) {
                        Log.e(TAG, "Error parsing venue", e);
                        event.setVenue("");
                    }

                    try {
                        double[] coordinates = EventHtmlParser.getVenueCoordinates(content);
                        event.setVenueCoordLat(coordinates[0]);
                        event.setVenueCoordLong(coordinates[1]);
                    }
                    catch (Exception e) {
                        Log.e(TAG, "Error parsing co-ordinates", e);
                        event.setVenueCoordLat(0);
                        event.setVenueCoordLong(0);
                    }

                    try {
                        String startDateString = eventJsonObject.getJSONObject(ApiConstants.EVENT_META).getString(ApiConstants.EVENT_START_DATE);
                        String endDateString = eventJsonObject.getJSONObject(ApiConstants.EVENT_META).getString(ApiConstants.EVENT_END_DATE);
                        //Log.d(TAG, startDateString + " - " + endDateString);
                        SimpleDateFormat eventDateFormat = new SimpleDateFormat(Event.DEFAULT_EVENT_DATE_PATTERN, Locale.getDefault());
                        event.setStartDate(eventDateFormat.parse(startDateString));
                        event.setEndDate(eventDateFormat.parse(endDateString));
                    }
                    catch (Exception e) {
                        Log.d(TAG, "Error parsing date.");
                        event.setStartDate(null);
                        event.setEndDate(null);
                    }

                    events.add(event);
                }
            }
        }
        catch (JSONException e) {
            Log.e(TAG, "Error parsing event content", e);
        }
        //Log.d(TAG, "events: " + events);
        return events;
    }

    public static Event parseEvent(JSONArray eventJsonArray) {

        Event event = new Event();

        try {

            if (eventJsonArray.length() == 1) {

                JSONObject eventJsonObject = eventJsonArray.getJSONObject(0);
                event = parseEventJsonObject(eventJsonObject);
            }
            else {
                Log.d(TAG, "Array contained more than one event!");
            }
        }
        catch (JSONException e) {
            Log.e(TAG, "Error parsing event", e);
        }

        return event;
    }

    public static Event parseEventJsonObject(JSONObject eventJsonObject) throws JSONException {

        Event event = new Event();
        event.setId(eventJsonObject.getInt(ApiConstants.EVENT_ID));
        event.setTitle(StringEscapeUtils.unescapeHtml4(eventJsonObject.getString(ApiConstants.EVENT_TITLE)));

        try {

            String startDateString = eventJsonObject.getJSONObject(ApiConstants.EVENT_META).getString(ApiConstants.EVENT_START_DATE);
            String endDateString = eventJsonObject.getJSONObject(ApiConstants.EVENT_META).getString(ApiConstants.EVENT_END_DATE);
            Log.d(TAG, startDateString + " - " + endDateString);

            SimpleDateFormat eventDateFormat = new SimpleDateFormat(Event.DEFAULT_EVENT_DATE_PATTERN, Locale.getDefault());

            event.setStartDate(eventDateFormat.parse(startDateString));
            event.setEndDate(eventDateFormat.parse(endDateString));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing start and end date", e);
            event.setStartDate(null);
            event.setEndDate(null);
        }

        String content = eventJsonObject.getString("content");
        event = parseEventContent(event, content);

        return event;
    }

    private static Event parseEventContent(Event event, String content) {

        try {
            event.setImageId(EventHtmlParser.getHeaderImageId(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing image id", e);
            event.setImageId(null);
        }

        try {
            event.setVenue(StringEscapeUtils.unescapeHtml4(EventHtmlParser.getVenue(content)));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing venue", e);
            event.setVenue("");
        }

        try {
            double[] coordinates = EventHtmlParser.getVenueCoordinates(content);
            event.setVenueCoordLat(coordinates[0]);
            event.setVenueCoordLong(coordinates[1]);
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing co-ordinates", e);
            event.setVenueCoordLat(0);
            event.setVenueCoordLong(0);
        }

        try {
            event.setPageSections(EventHtmlParser.getPageSections(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing event sections", e);
        }

        try {
            event.setSpeakers(EventHtmlParser.getSpeakers(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing speakers", e);
        }

        try {
            event.setPartners(EventHtmlParser.getPartners(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing partners", e);
        }

        try {
            event.setAgendaContent(EventHtmlParser.getAgendaContent(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing agenda content", e);
        }

        try {
            event.setFaqs(EventHtmlParser.getFaqs(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing FAQs", e);
        }

        try {
            event.setRegistrationURL(EventHtmlParser.getRegistrationURL(content));
        }
        catch (Exception e) {
            Log.e(TAG, "Error parsing registration url", e);
        }

        return event;
    }
}
