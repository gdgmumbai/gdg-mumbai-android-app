package org.gdgmumbai.api;

/**
 * Created by Ammar Githam on 19-10-2014.
 */
class ApiConstants {

    private static final String BASE_URL = "http://www.gdgmumbai.org/wp-json";
    public static final String EVENTS_URL = BASE_URL + "/posts?type=page&filter[cat]=60";
    public static final String EVENT_ID_URL = BASE_URL + "/posts?type=page&filter[page_id]=";
    public static final String IMAGES_URL = BASE_URL + "/media/";

    // Events reated constants
    public static final String EVENT_ID = "ID";
    public static final String EVENT_TITLE = "title";
    public static final String EVENT_VENUE_COORD_LAT = "venueCoordinatesLat";
    public static final String EVENT_VENUE_COORD_LONG = "venueCoordinatesLong";
    public static final String EVENT_DATE = "date";
    public static final String EVENT_THUMBNAIL_URL = "thumbnailUrl";
    public static final String EVENT_FULL_IMAGE_URL = "fullImageUrl";
    public static final String EVENT_IMAGE_BASE_URL = "http://ammar786-1.github.io/img/events";
    public static final String EVENT_SHORT_DESCRIPTION = "shortDesc";
    public static final String EVENT_LONG_DESCRIPTION = "longDesc";
    public static final String EVENT_TIMESLOTS = "timeslots";
    public static final String EVENT_META = "meta";
    public static final String EVENT_START_DATE = "start_date";
    public static final String EVENT_END_DATE = "end_date";


    // Session related constants
    public static final String TIMESLOT_START_TIME = "startTime";
    public static final String TIMESLOT_END_TIME = "endTime";
    public static final String TIMESLOT_SESSION = "session";
}
