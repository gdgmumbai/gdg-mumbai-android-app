package org.gdgmumbai.events;

import android.content.Context;
import android.content.SharedPreferences;

import org.gdgmumbai.api.JSONParser;
import org.gdgmumbai.utils.AppTools;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by agitham on 15/7/2015.
 */
public class EventsCache {

    public static final String KEY_CACHED_EVENTS_JSON = "cached_events_json";
    public static final String KEY_CACHED_IMAGES = "cached_images";

    public static ArrayList<Event> getCachedEvents(Context context) {

        ArrayList<Event> events = new ArrayList<>();

        if (context == null)
            return events;

        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        String eventsJson = sharedPreferences.getString(KEY_CACHED_EVENTS_JSON, "");

        if (!AppTools.isNullOrWhitespace(eventsJson)) {

            try {
                events = JSONParser.parseEvents(new JSONArray(eventsJson));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return events;
    }
}
