package org.gdgmumbai.events;

import org.gdgmumbai.common.Faq;
import org.gdgmumbai.common.PageSection;
import org.gdgmumbai.common.Partner;
import org.gdgmumbai.common.Speaker;

import java.util.ArrayList;
import java.util.Date;

public class Event {

    public static final String DEFAULT_EVENT_DATE_PATTERN = "MMMM d, yyyy h:mm a";

    private int id;
    private String title;
    private Date startDate;
    private Date endDate;
    private String venue;
    private double venueCoordLat;
    private double venueCoordLong;
    private String imageId;
    private String imageThumbnailUrl;
    private String registrationURL;
    private String agendaContent;
    private String eventDetails;
    private ArrayList<Speaker> speakers;
    private ArrayList<Partner> partners;
    private ArrayList<PageSection> pageSections;
    private ArrayList<Faq> faqs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public double getVenueCoordLat() {
        return venueCoordLat;
    }

    public void setVenueCoordLat(double venueCoordLat) {
        this.venueCoordLat = venueCoordLat;
    }

    public double getVenueCoordLong() {
        return venueCoordLong;
    }

    public void setVenueCoordLong(double venueCoordLong) {
        this.venueCoordLong = venueCoordLong;
    }

    public ArrayList<Speaker> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(ArrayList<Speaker> speakers) {
        this.speakers = speakers;
    }

    public ArrayList<Partner> getPartners() {
        return partners;
    }

    public void setPartners(ArrayList<Partner> partners) {
        this.partners = partners;
    }

    public ArrayList<PageSection> getPageSections() {
        return pageSections;
    }

    public void setPageSections(ArrayList<PageSection> pageSections) {
        this.pageSections = pageSections;
    }

    public String getRegistrationURL() {
        return registrationURL;
    }

    public void setRegistrationURL(String registrationURL) {
        this.registrationURL = registrationURL;
    }

    public String getAgendaContent() {
        return agendaContent;
    }

    public void setAgendaContent(String agendaContent) {
        this.agendaContent = agendaContent;
    }

    public ArrayList<Faq> getFaqs() {
        return faqs;
    }

    public void setFaqs(ArrayList<Faq> faqs) {
        this.faqs = faqs;
    }

    public String getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(String eventDetails) {
        this.eventDetails = eventDetails;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Event && ((Event) o).id == id;

    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }
}
