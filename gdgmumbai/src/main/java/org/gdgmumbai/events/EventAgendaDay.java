package org.gdgmumbai.events;

/**
 * Created by Ammar Githam on 19-04-2015.
 */
public class EventAgendaDay {

    private String title;
    private String subtitle;
    private String sessionsContent;
    //private ArrayList<Session> sessions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    //public ArrayList<Session> getSessions() {
    //    return sessions;
    //}

    //public void setSessions(ArrayList<Session> sessions) {
    //    this.sessions = sessions;
    //}

    @Override
    public String toString() {
        return "EventAgendaDay{" +
                "title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", sessionsContent=" + sessionsContent +
                '}';
    }

    public String getSessionsContent() {
        return sessionsContent;
    }

    public void setSessionsContent(String sessionsContent) {
        this.sessionsContent = sessionsContent;
    }
}
