package org.gdgmumbai.events;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import org.gdgmumbai.R;
import org.gdgmumbai.api.EventHtmlParser;
import org.gdgmumbai.common.Session;
import org.gdgmumbai.utils.RequestImageLoaderSingleton;

import java.util.ArrayList;

/**
 * Created by Ammar Githam on 18-04-2015.
 */
public class EventAgendaDayFragment extends Fragment {

    private ImageLoader mImageLoader;
    private ArrayList<Session> sessions;
    private String sessionsContent;

    public static EventAgendaDayFragment newInstance(EventAgendaDay eventAgendaDay) {

        EventAgendaDayFragment fragment = new EventAgendaDayFragment();

        Bundle bundle = new Bundle();
        bundle.putString("title", eventAgendaDay.getTitle());
        bundle.putString("subtitle", eventAgendaDay.getSubtitle());
        bundle.putString("sessionsContent", eventAgendaDay.getSessionsContent());
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageLoader = RequestImageLoaderSingleton.getInstance(getActivity().getApplicationContext()).getImageLoader();

        sessionsContent = getArguments().getString("sessionsContent");
        sessions = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.EventTheme);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        View view = localInflater.inflate(R.layout.fragment_event_agenda_day, container, false);

        RecyclerView mSessionsRecyclerView = (RecyclerView) view.findViewById(R.id.session_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mSessionsRecyclerView.setLayoutManager(linearLayoutManager);
        SessionsAdapter mSessionsAdapter = new SessionsAdapter(sessions, mImageLoader);
        mSessionsRecyclerView.setAdapter(mSessionsAdapter);

        try {
            sessions = EventHtmlParser.getSessionsFromContent(sessionsContent);
            mSessionsAdapter.updateData(sessions);
            mSessionsAdapter.notifyDataSetChanged();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
}
