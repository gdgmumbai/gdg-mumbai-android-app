package org.gdgmumbai.events;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Ammar Githam on 18-04-2015.
 */
class AgendaDaysPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<EventAgendaDay> eventAgendaDays;

    AgendaDaysPagerAdapter(FragmentManager fragmentManager, ArrayList<EventAgendaDay> eventAgendaDays) {
        super(fragmentManager);
        this.eventAgendaDays = eventAgendaDays;
    }

    @Override
    public Fragment getItem(int position) {
        return EventAgendaDayFragment.newInstance(eventAgendaDays.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return eventAgendaDays.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return eventAgendaDays.size();
    }
}
