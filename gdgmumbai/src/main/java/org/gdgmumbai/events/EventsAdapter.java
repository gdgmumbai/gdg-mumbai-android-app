package org.gdgmumbai.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.gdgmumbai.R;
import org.gdgmumbai.api.ApiHelperImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    private static final String TAG = "EventsAdapter";
    private ArrayList<Event> events;
    private ImageLoader mImageLoader;
    private int lastPosition = -1;
    private RequestQueue mRequestQueue;

    public EventsAdapter(ArrayList<Event> events, ImageLoader mImageLoader, RequestQueue requestQueue) {
        this.events = events;
        this.mImageLoader = mImageLoader;
        mRequestQueue = requestQueue;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final EventViewHolder holder, int position) {

        final Event event = events.get(position);

        holder.id = event.getId();
        holder.title.setText(event.getTitle());
        holder.eventDetails = event.getEventDetails();

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());
            String startDate = dateFormat.format(event.getStartDate());
            String endDate = dateFormat.format(event.getEndDate());

            SimpleDateFormat dateDisplayFormat = new SimpleDateFormat("MMMM d", Locale.getDefault());
            SimpleDateFormat endDisplayFormat = new SimpleDateFormat("d", Locale.getDefault());

            if (startDate.equalsIgnoreCase(endDate)) {
                //Single Day event. Show only the date as March 15
                holder.date.setText(dateDisplayFormat.format(event.getStartDate()));
            } else {

                // If event days are in the same month of the year.
                SimpleDateFormat sameMonthDateFormat = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
                startDate = sameMonthDateFormat.format(event.getStartDate());
                endDate = sameMonthDateFormat.format(event.getEndDate());

                if (startDate.equalsIgnoreCase(endDate)) {
                    // Show as March 14 - 15
                    String startDisplayString = dateDisplayFormat.format(event.getStartDate());
                    String endDisplayString = endDisplayFormat.format(event.getEndDate());
                    holder.date.setText(startDisplayString + " - " + endDisplayString);
                } else {
                    //Multiple days event which spans multiple months. Show as March 15 - April 16
                    holder.date.setText(dateDisplayFormat.format(event.getStartDate()) + " - " + dateDisplayFormat.format(event.getEndDate()));
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "Error in parsing date.");
            //e.printStackTrace();
            holder.date.setVisibility(View.GONE);
        }

        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.slide_up_left);
            holder.itemView.startAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (event.getImageId() != null) {
                        setImageToHolder(holder, event);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            lastPosition = position;
        } else {
            if (event.getImageId() != null) {
                setImageToHolder(holder, event);
            }
        }
    }

    private void setImageToHolder(final EventViewHolder holder, final Event event) {
        if (event.getImageThumbnailUrl() != null) {
            holder.image.setImageUrl(event.getImageThumbnailUrl(), mImageLoader);
            return;
        }
        ApiHelperImpl apiHelper = new ApiHelperImpl();
        apiHelper.requestImageUrls(holder.itemView.getContext(), event.getImageId(), mRequestQueue, new ApiHelperImpl.ImageRequestCallback() {
            @Override
            public void onImageUrlsReceived(int id, String... urls) {
                if (holder.id == id) {
                    event.setImageThumbnailUrl(urls[1]);
                    holder.image.setImageUrl(urls[1], mImageLoader);
                    holder.imageUrl = urls[1];
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "Error fetching urls", volleyError);
            }
        }, event.getId(), false);
    }

    @Override
    public void onViewRecycled(EventViewHolder holder) {
        holder.image.setImageUrl("", mImageLoader);
        holder.imageUrl = null;
    }

    @Override
    public long getItemId(int position) {
        return events.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public void updateData(ArrayList<Event> events) {
        this.events = events;
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int id;
        String eventDetails;
        String imageUrl;
        NetworkImageView image;
        TextView date;
        TextView title;
        TextView subtitle;

        EventViewHolder(View itemView) {
            super(itemView);

            itemView.findViewById(R.id.event_card).setOnClickListener(this);

            date = (TextView) itemView.findViewById(R.id.date);
            image = (NetworkImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        }

        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.event_card) {
                Intent intent = new Intent(view.getContext(), EventDetailActivity.class);

                String date = itemView.findViewById(R.id.date) != null ? ((TextView) itemView.findViewById(R.id.date)).getText().toString() : "";
                String title = itemView.findViewById(R.id.title) != null ? ((TextView) itemView.findViewById(R.id.title)).getText().toString() : "";
                String subtitle = itemView.findViewById(R.id.subtitle) != null ? ((TextView) itemView.findViewById(R.id.subtitle)).getText().toString() : "";

                Bundle bundle = new Bundle();
                bundle.putInt(EventDetailFragment.BUNDLE_KEY_ID, id);
                bundle.putString(EventDetailFragment.BUNDLE_KEY_DATE, date);
                bundle.putString(EventDetailFragment.BUNDLE_KEY_TITLE, title);
                bundle.putString(EventDetailFragment.BUNDLE_KEY_SUBTITLE, subtitle);
                bundle.putString(EventDetailFragment.BUNDLE_KEY_IMAGE_URL, imageUrl);
                bundle.putString(EventDetailFragment.BUNDLE_KEY_EVENT_DETAILS, eventDetails);
                intent.putExtras(bundle);

                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                //    view.getContext().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(mainActivity, itemView.findViewById(R.id.image), "imageView").toBundle());
                view.getContext().startActivity(intent);
            }
        }
    }
}
