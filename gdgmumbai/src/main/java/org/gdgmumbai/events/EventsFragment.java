package org.gdgmumbai.events;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import org.gdgmumbai.R;
import org.gdgmumbai.api.ApiHelper;
import org.gdgmumbai.api.ApiHelperImpl;
import org.gdgmumbai.common.MainActivity;
import org.gdgmumbai.utils.RequestImageLoaderSingleton;

import java.util.ArrayList;

public class EventsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "EventsFragment";
    private static final String EVENTS_STATE = "events_state";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<Event> mEventList;
    private RequestQueue mRequestQueue;
    private EventsAdapter mEventsAdapter;
    private ImageLoader mImageLoader;
    private LinearLayoutManager linearLayoutManager;

    public static EventsFragment newInstance() {
        return new EventsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() == null) {
            return;
        }

        mRequestQueue = RequestImageLoaderSingleton.getInstance(getActivity().getApplicationContext()).getRequestQueue();
        mEventList = new ArrayList<>();
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<>(10);

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            @Override
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getActivity() == null || !(getActivity() instanceof MainActivity))
            return null;

        if (((MainActivity) getActivity()).getToolbar() != null) {
            ((MainActivity) getActivity()).getToolbar().setBackgroundColor(getResources().getColor(R.color.events_primary));
        }

        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getResources().getStringArray(R.array.navigation_items_array)[0]);
        }

        DrawerLayout mDrawerLayout = ((MainActivity) getActivity()).getDrawerLayout();
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.events_primary_dark));

        Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.EventTheme);
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        View mRootView = localInflater.inflate(R.layout.fragment_events, container, false);
        RecyclerView mEventsRecyclerView = (RecyclerView) mRootView.findViewById(R.id.events);

        mSwipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.refreshlayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.events_primary, R.color.red, R.color.events_primary, R.color.red);

        mEventList = EventsCache.getCachedEvents(getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity());
        mEventsRecyclerView.setLayoutManager(linearLayoutManager);
        mEventsAdapter = new EventsAdapter(mEventList, mImageLoader, mRequestQueue);
        mEventsRecyclerView.setAdapter(mEventsAdapter);

        if (savedInstanceState == null) {
            // Workaround to show initial loading animation.
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    refresh(false);
                }
            });
        }

        if (savedInstanceState != null && savedInstanceState.getParcelable(EVENTS_STATE) != null) {
            linearLayoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(EVENTS_STATE));
        }

        return mRootView;
    }

    @Override
    public void onRefresh() {
        // If swiped down to refresh then invalidate cache and force refresh
        refresh(true);
    }

    public void setData(ArrayList<Event> events) {
        mEventList = events;
        mEventsAdapter.updateData(events);
        mEventsAdapter.notifyDataSetChanged();
    }

    public void refresh(boolean forceRefresh) {

        mSwipeRefreshLayout.setEnabled(true);

        ApiHelper apiHelper = new ApiHelperImpl();
        apiHelper.requestAllEvents(getActivity(), mRequestQueue, new ApiHelperImpl.EventsRequestCallback() {
            @Override
            public void onEventsReceived(ArrayList<Event> events) {
                if (events != null) {
                    if (!events.equals(mEventList)) {
                        setData(events);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), getString(R.string.network_error), Toast.LENGTH_LONG).show();
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, TAG, forceRefresh);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mRequestQueue != null)
            mRequestQueue.cancelAll(TAG);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EVENTS_STATE, linearLayoutManager.onSaveInstanceState());
    }
}
