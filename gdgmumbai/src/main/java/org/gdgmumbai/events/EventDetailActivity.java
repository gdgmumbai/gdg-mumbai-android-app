package org.gdgmumbai.events;

import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import org.gdgmumbai.R;
import org.gdgmumbai.common.BaseActivity;
import org.gdgmumbai.utils.AppTools;

/**
 * Created by agitham on 29/9/2014.
 */
public class EventDetailActivity extends BaseActivity {

    private static final String TAG = "EventDetailActivity";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_event_detail;
    }

    @Override
    protected int getThemeResource() {
        return R.style.EventTheme;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getToolbar().setBackgroundColor(getResources().getColor(android.R.color.transparent));

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.semitransparent_black));
        }

        AppTools.setMargins(getToolbar(), 0, AppTools.getStatusBarHeight(getResources()), 0, 0);

        String tag = EventDetailFragment.TAG;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag) != null ? getSupportFragmentManager().findFragmentByTag(tag) : EventDetailFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment, tag)
                .commit();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getToolbar().setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    finishAfterTransition();
                else finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
