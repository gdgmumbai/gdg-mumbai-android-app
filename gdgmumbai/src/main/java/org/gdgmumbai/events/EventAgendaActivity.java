package org.gdgmumbai.events;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import org.gdgmumbai.R;
import org.gdgmumbai.api.EventHtmlParser;
import org.gdgmumbai.common.BaseActivity;
import org.gdgmumbai.utils.customviews.SlidingTabLayout;

import java.util.ArrayList;

/**
 * Created by Ammar Githam on 18-04-2015.
 */
public class EventAgendaActivity extends BaseActivity {

    private static final String TAG = "AgendaActivity";
    private SlidingTabLayout tabLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_event_agenda;
    }

    @Override
    protected int getThemeResource() {
        return R.style.EventTheme;
    }

    public SlidingTabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.EventTheme);
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        String eventTitle = bundle.getString("event_title");
        String content = bundle.getString("content");

        if (getToolbar() != null) {

            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getToolbar().setBackgroundColor(getResources().getColor(R.color.events_primary));
            getToolbar().setTitle(eventTitle);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.events_primary_dark));
            findViewById(R.id.bottom_shadow).setVisibility(View.GONE);
        }

        tabLayout = (SlidingTabLayout) findViewById(R.id.tabs);
        //tabLayout.setDistributeEvenly(true); // To make the Tabs Fixed

        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        tabLayout.setBackgroundColor(getResources().getColor(R.color.events_primary));
        tabLayout.setCustomTabView(R.layout.event_agenda_tab, R.id.tab_text);

        new EventAgendaDaysParserAsyncTask().execute(content);
        /*ArrayList<EventAgendaDay> eventAgendaDays = new ArrayList<>();

        try {
            eventAgendaDays = EventHtmlParser.getAgendaDays(content);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    finishAfterTransition();
                else finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private class EventAgendaDaysParserAsyncTask extends AsyncTask<String, Void, ArrayList<EventAgendaDay>> {

        @Override
        protected ArrayList<EventAgendaDay> doInBackground(String... strings) {

            ArrayList<EventAgendaDay> eventAgendaDays = new ArrayList<>();

            if (strings.length > 0) {

                String content = strings[0];

                try {
                    eventAgendaDays = EventHtmlParser.getAgendaDays(content);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return eventAgendaDays;
        }

        @Override
        protected void onPostExecute(ArrayList<EventAgendaDay> eventAgendaDays) {

            AgendaDaysPagerAdapter mAgendaDaysPagerAdapter = new AgendaDaysPagerAdapter(getSupportFragmentManager(), eventAgendaDays);
            ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(mAgendaDaysPagerAdapter);
            tabLayout.setViewPager(mViewPager);
        }
    }
}
