package org.gdgmumbai.events;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.gdgmumbai.R;
import org.gdgmumbai.api.ApiHelperImpl;
import org.gdgmumbai.api.JSONParser;
import org.gdgmumbai.common.ButtonLink;
import org.gdgmumbai.common.Faq;
import org.gdgmumbai.common.PageSection;
import org.gdgmumbai.common.Partner;
import org.gdgmumbai.common.Speaker;
import org.gdgmumbai.utils.AppTools;
import org.gdgmumbai.utils.RequestImageLoaderSingleton;
import org.gdgmumbai.utils.customviews.ObservableScrollView;
import org.gdgmumbai.utils.customviews.WorkaroundMapFragment;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by agitham on 29/9/2014.
 */
public class EventDetailFragment extends Fragment implements ObservableScrollView.Callbacks, WorkaroundMapFragment.OnMapReadyListener, WorkaroundMapFragment.OnTouchListener {

    public static final String BUNDLE_KEY_ID = "id";
    public static final String BUNDLE_KEY_TITLE = "title";
    public static final String BUNDLE_KEY_SUBTITLE = "subtitle";
    public static final String BUNDLE_KEY_DATE = "date";
    public static final String BUNDLE_KEY_IMAGE_URL = "imageUrl";
    public static final String BUNDLE_KEY_EVENT_DETAILS = "eventDetails";

    private static final float PHOTO_ASPECT_RATIO = 1.7777777f;
    private static final float GAP_FILL_DISTANCE_MULTIPLIER = 1.5f;
    public static final String TAG = "EventDetailFragment";
    private static final String SCROLL_VIEW_X = "scroll_view_x";
    private static final String SCROLL_VIEW_Y = "scroll_view_y";

    private int mHeaderTopClearance;
    private ObservableScrollView mRootScrollView;
    private View mHeaderContentBox;
    private int mHeaderHeightPixels;
    private boolean mHasPhoto = true;
    private int mPhotoHeightPixels;
    private NetworkImageView mPhotoView;
    private View mPhotoViewContainer;
    private View mHeaderBackgroundBox;
    private View mDetailsContainer;
    private View mHeaderBox;
    private boolean mGapFillShown;
    private View mHeaderShadow;
    private TextView mDateTextView;
    private TextView mTitleTextView;
    private TextView mSubtitleTextView;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private LinearLayout mPageSectionContainer;
    private LinearLayout mPartnersContainer;
    private View mPartnersSectionTitle;
    private LinearLayout mSpeakersContainer;
    private View mSpeakersSectionTitle;
    private View mSpeakersSectionDivider;
    private LinearLayout mFaqsContainer;
    private View mFaqsSectionTitle;
    private View mFaqsSectionDivider;
    private LayoutInflater mLocalInflater;
    private int mScrollViewPositionX;
    private int mScrollViewPositionY;
    private boolean isAgendaAvailable;
    private Event event;

    public static EventDetailFragment newInstance() {
        return new EventDetailFragment();
    }

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener;
    private GoogleMap mMap;
    private FloatingActionButton mRegistrationFAB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                recomputeScrollingMetrics();
            }
        };

        mRequestQueue = RequestImageLoaderSingleton.getInstance(getActivity().getApplicationContext()).getRequestQueue();
        mImageLoader = RequestImageLoaderSingleton.getInstance(getActivity().getApplicationContext()).getImageLoader();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.EventTheme);
        //mLocalInflater = getActivity().getLayoutInflater()/*.cloneInContext(contextThemeWrapper)*/;
        mLocalInflater = inflater;

        View mRootView = mLocalInflater.inflate(R.layout.fragment_event_detail, container, false);
        mRootScrollView = (ObservableScrollView) mRootView.findViewById(R.id.rootView);
        mHeaderBox = mRootScrollView.findViewById(R.id.event_header_fl);
        mHeaderContentBox = mRootScrollView.findViewById(R.id.event_header_ll);
        mHeaderBackgroundBox = mRootScrollView.findViewById(R.id.header_background);
        mHeaderShadow = mRootScrollView.findViewById(R.id.header_shadow);
        mPhotoViewContainer = mRootScrollView.findViewById(R.id.event_photo_container);
        mPhotoView = (NetworkImageView) mRootScrollView.findViewById(R.id.event_header_image);
        mDetailsContainer = mRootScrollView.findViewById(R.id.event_details_ll);
        mPageSectionContainer = (LinearLayout) mRootScrollView.findViewById(R.id.event_page_sections);
        mDateTextView = (TextView) mRootScrollView.findViewById(R.id.date);
        mTitleTextView = (TextView) mRootScrollView.findViewById(R.id.title);
        mSubtitleTextView = (TextView) mRootScrollView.findViewById(R.id.subtitle);
        mSpeakersContainer = (LinearLayout) mRootScrollView.findViewById(R.id.speakers_container);
        mPartnersContainer = (LinearLayout) mRootScrollView.findViewById(R.id.partners_container);
        mPartnersSectionTitle = mRootScrollView.findViewById(R.id.partners_section_title);
        mSpeakersSectionTitle = mRootScrollView.findViewById(R.id.speakers_section_title);
        mSpeakersSectionDivider = mRootScrollView.findViewById(R.id.speakers_section_divider);
        mFaqsContainer = (LinearLayout) mRootScrollView.findViewById(R.id.faqs_container);
        mFaqsSectionDivider = mRootScrollView.findViewById(R.id.faqs_section_divider);
        mFaqsSectionTitle = mRootScrollView.findViewById(R.id.faqs_section_title);
        mRegistrationFAB = (FloatingActionButton) mRootView.findViewById(R.id.fab);

        String mapFragmentTag = "WorkaroundMapFragment";
        WorkaroundMapFragment mMapFragment = getChildFragmentManager().findFragmentByTag(mapFragmentTag) != null ? (WorkaroundMapFragment) getChildFragmentManager().findFragmentByTag(mapFragmentTag) : WorkaroundMapFragment.newInstance();

        if (savedInstanceState == null) {
            // First incarnation of this activity.
            mMapFragment.setRetainInstance(true);
        }
        else {
            // Reincarnated activity. The obtained map is the same map instance in the previous
            // activity life cycle. There is no need to reinitialize it.
            mMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                }
            });
        }

        try {
            getChildFragmentManager().beginTransaction().add(R.id.map, mMapFragment).commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (savedInstanceState != null) {
            mScrollViewPositionX = savedInstanceState.getInt(SCROLL_VIEW_X);
            mScrollViewPositionY = savedInstanceState.getInt(SCROLL_VIEW_Y);
        }

        setData();
        setUpInitialMetrics();

        return mRootView;
    }

    private void setData() {

        if (getActivity() == null)
            return;

        Bundle bundle = getActivity().getIntent().getExtras();

        String title = bundle.getString(BUNDLE_KEY_TITLE);
        String subtitle = bundle.getString(BUNDLE_KEY_SUBTITLE);
        String date = bundle.getString(BUNDLE_KEY_DATE);
        String imageUrl = bundle.getString(BUNDLE_KEY_IMAGE_URL);
        String eventDetails = bundle.getString(BUNDLE_KEY_EVENT_DETAILS);

        mTitleTextView.setText(title != null ? title : "");
        mSubtitleTextView.setText(subtitle != null ? subtitle : "");
        mDateTextView.setText(date != null ? date : "");

        ((LinearLayout) mHeaderContentBox).removeView(mSubtitleTextView);

        if (imageUrl != null) {
            mPhotoView.setImageUrl(imageUrl, mImageLoader);
        }

        //int id = bundle.getInt(BUNDLE_KEY_ID, 0);

        JSONObject eventJsonObject = null;

        try {
            eventJsonObject = new JSONObject(eventDetails);
            new EventJsonObjectParserAsyncTask().execute(eventJsonObject);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        if (eventJsonObject == null) {
            if (getActivity() != null)
                Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_LONG).show();
        }
    }

    private void setEventData(Event event) {

        if (event != null) {
            this.event = event;
            if (event.getImageId() != null) {
                ApiHelperImpl apiHelper = new ApiHelperImpl();
                apiHelper.requestImageUrls(getContext(), event.getImageId(), mRequestQueue, new ApiHelperImpl.ImageRequestCallback() {
                    @Override
                    public void onImageUrlsReceived(int id, String... urls) {
                        mPhotoView.setImageUrl(urls[1], mImageLoader);
                    }

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "Error fetching urls", volleyError);
                    }
                }, event.getId(), false);
            }
            mTitleTextView.setText(event.getTitle());
            setCoordinatesToMap(event.getVenueCoordLat(), event.getVenueCoordLong(), event.getVenue());
            setDates(event.getStartDate(), event.getEndDate());
            setPageSections(event.getPageSections());
            setSpeakers(event.getSpeakers());
            setFaqs(event.getFaqs());
            setPartners(event.getPartners());
            isAgendaAvailable = !AppTools.isNullOrWhitespace(event.getAgendaContent());
            getActivity().invalidateOptionsMenu();
            setRegistrationURL(event.getRegistrationURL());
        }
        mDetailsContainer.setVisibility(View.VISIBLE);
        mRootScrollView.smoothScrollTo(mScrollViewPositionX, mScrollViewPositionY);
    }

    private void setCoordinatesToMap(double venueCoordLat, double venueCoordLong, String mapTitle) {

        if (venueCoordLat != 0 && venueCoordLong != 0) {
            try {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(venueCoordLat, venueCoordLong), 15.0f));
                mMap.addMarker(new MarkerOptions().position(new LatLng(venueCoordLat, venueCoordLong)).title(AppTools.isNullOrWhitespace(mapTitle) ? "" : mapTitle)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            }
            catch (Exception e) {
                Log.d(TAG, "Google Play Services missing!");
            }
        }
    }

    private void setDates(Date startDate, Date endDate) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());
            String startDateString = dateFormat.format(startDate);
            String endDateString = dateFormat.format(endDate);

            SimpleDateFormat dateDisplayFormat = new SimpleDateFormat("MMMM d", Locale.getDefault());
            SimpleDateFormat endDisplayFormat = new SimpleDateFormat("d", Locale.getDefault());

            if (startDateString.equalsIgnoreCase(endDateString)) {
                //Single Day event. Show only the date as March 15
                mDateTextView.setText(dateDisplayFormat.format(startDate));
            }
            else {

                // If event days are in the same month of the year.
                SimpleDateFormat sameMonthDateFormat = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
                startDateString = sameMonthDateFormat.format(startDate);
                endDateString = sameMonthDateFormat.format(endDate);

                if (startDateString.equalsIgnoreCase(endDateString)) {
                    // Show as March 14 - 15
                    String startDisplayString = dateDisplayFormat.format(startDate);
                    String endDisplayString = endDisplayFormat.format(endDate);
                    mDateTextView.setText(startDisplayString + " - " + endDisplayString);
                }
                else {
                    //Multiple days event which spans multiple months. Show as March 15 - April 16
                    mDateTextView.setText(dateDisplayFormat.format(startDate) + " - " + dateDisplayFormat.format(endDate));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            mDateTextView.setVisibility(View.GONE);
        }
    }

    private void setRegistrationURL(final String registrationURL) {

        if (AppTools.isNullOrWhitespace(registrationURL)) {
            mRegistrationFAB.setVisibility(View.GONE);
            return;
        }

        mRegistrationFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(registrationURL));
                startActivity(browserIntent);
            }
        });
    }

    private void setPartners(List<Partner> partners) {

        // TODO Will it be better if these view are hidden by default and then shown if content is there?
        if (partners.isEmpty()) {

            mPartnersContainer.setVisibility(View.GONE);
            mPartnersSectionTitle.setVisibility(View.GONE);
            return;
        }

        for (Partner partner : partners) {
            if (getActivity() != null) {
                View partnerChildView = mLocalInflater.inflate(R.layout.event_partner, mPartnersContainer, false);
                NetworkImageView partnerImageView = (NetworkImageView) partnerChildView.findViewById(R.id.partner_image);
                TextView partnerNameTextView = (TextView) partnerChildView.findViewById(R.id.partner_name);
                partnerImageView.setImageUrl(partner.getImageUrl(), mImageLoader);
                partnerNameTextView.setText(partner.getName());
                mPartnersContainer.addView(partnerChildView);
            }
        }
    }

    private void setSpeakers(List<Speaker> speakers) {

        // TODO Will it be better if these view are hidden by default and then shown if content is there?
        if (speakers.isEmpty()) {

            mSpeakersContainer.setVisibility(View.GONE);
            mSpeakersSectionTitle.setVisibility(View.GONE);
            mSpeakersSectionDivider.setVisibility(View.GONE);
            return;
        }

        for (Speaker speaker : speakers) {

            if (getActivity() != null) {
                View speakerChildView = mLocalInflater.inflate(R.layout.event_speaker, mSpeakersContainer, false);
                NetworkImageView speakerImageView = (NetworkImageView) speakerChildView.findViewById(R.id.speaker_image);
                TextView speakerNameTextView = (TextView) speakerChildView.findViewById(R.id.speaker_name);
                speakerImageView.setImageUrl(speaker.getImageURL(), mImageLoader);
                speakerNameTextView.setText(speaker.getName());
                mSpeakersContainer.addView(speakerChildView);
            }
        }
    }

    private void setPageSections(Iterable<PageSection> pageSections) {

        for (PageSection pageSection : pageSections) {
            if (!AppTools.isNullOrWhitespace(pageSection.getContent())) {
                String heading = pageSection.getHeading();
                String content = pageSection.getContent();

                if (getActivity() != null) {
                    ViewGroup pageSectionChildViewGroup = (ViewGroup) mLocalInflater.inflate(R.layout.page_section_normal, mPageSectionContainer, false);
                    TextView headingTextView = (TextView) pageSectionChildViewGroup.findViewById(R.id.heading);
                    TextView contentTextView = (TextView) pageSectionChildViewGroup.findViewById(R.id.section_content);
                    headingTextView.setText(heading);
                    contentTextView.setText(content);

                    LinearLayout mButtonsContainer = (LinearLayout) pageSectionChildViewGroup.findViewById(R.id.buttons_container);

                    if (pageSection.getButtonLinks().size() > 2)
                        mButtonsContainer.setOrientation(LinearLayout.VERTICAL);

                    for (final ButtonLink buttonLink : pageSection.getButtonLinks()) {
                        if (getActivity() != null) {
                            View buttonChildView = getActivity().getLayoutInflater().inflate(R.layout.button_raised, pageSectionChildViewGroup, false);
                            Button button = (Button) buttonChildView.findViewById(R.id.button);
                            button.setText(buttonLink.getLabel());
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(buttonLink.getUrl()));
                                    startActivity(browserIntent);
                                }
                            });
                            mButtonsContainer.addView(buttonChildView);
                        }
                    }

                    mPageSectionContainer.addView(pageSectionChildViewGroup);
                }
            }
        }
    }

    private void setFaqs(List<Faq> faqs) {

        // TODO Will it be better if these view are hidden by default and then shown if content is there?
        if (faqs.isEmpty()) {

            mFaqsContainer.setVisibility(View.GONE);
            mFaqsSectionTitle.setVisibility(View.GONE);
            mFaqsSectionDivider.setVisibility(View.GONE);
            return;
        }

        for (Faq faq : faqs) {

            if (getActivity() != null) {
                View faqChildView = mLocalInflater.inflate(R.layout.event_faq, mFaqsContainer, false);
                TextView questionTextView = (TextView) faqChildView.findViewById(R.id.question);
                TextView answerTextView = (TextView) faqChildView.findViewById(R.id.answer);
                questionTextView.setText(faq.getQuestion());
                answerTextView.setText(faq.getAnswer());
                mFaqsContainer.addView(faqChildView);
            }
        }
    }

    private void setUpInitialMetrics() {

        mRootScrollView.addCallbacks(this);
        ViewTreeObserver vto = mRootScrollView.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(mGlobalLayoutListener);
            recomputeScrollingMetrics();
        }
    }

    private void recomputeScrollingMetrics() {

        if (getActivity() == null)
            return;

        int actionBarSize = AppTools.getActionBarSize(getActivity());
        mHeaderTopClearance = actionBarSize - mHeaderContentBox.getPaddingTop() + AppTools.getStatusBarHeight(getResources());
        mHeaderHeightPixels = mHeaderContentBox.getHeight();
        mPhotoHeightPixels = mHeaderTopClearance;

        if (mHasPhoto) {
            mPhotoHeightPixels = (int) (mPhotoView.getWidth() / PHOTO_ASPECT_RATIO);
            mPhotoHeightPixels = Math.min(mPhotoHeightPixels, mRootScrollView.getHeight() * 2 / 3);
        }

        ViewGroup.LayoutParams lp;
        lp = mPhotoViewContainer.getLayoutParams();

        if (lp.height != mPhotoHeightPixels) {
            lp.height = mPhotoHeightPixels;
            mPhotoViewContainer.setLayoutParams(lp);
        }

        lp = mHeaderBackgroundBox.getLayoutParams();

        if (lp.height != mHeaderHeightPixels) {
            lp.height = mHeaderHeightPixels;
            mHeaderBackgroundBox.setLayoutParams(lp);
        }

        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)
                mDetailsContainer.getLayoutParams();
        if (mlp.topMargin != mHeaderHeightPixels + mPhotoHeightPixels) {
            mlp.topMargin = mHeaderHeightPixels + mPhotoHeightPixels;
            mDetailsContainer.setLayoutParams(mlp);
        }

        onScrollChanged(0, 0); // trigger scroll handling
    }

    @Override
    public void onScrollChanged(int deltaX, int deltaY) {

        Activity activity = getActivity();

        if (activity == null) {
            return;
        }

        // Reposition the header bar -- it's normally anchored to the top of the content,
        // but locks to the top of the screen on scroll
        int scrollY = mRootScrollView.getScrollY();

        float newTop = Math.max(mPhotoHeightPixels, scrollY + mHeaderTopClearance);
        mHeaderBox.setTranslationY(newTop);

        mHeaderBackgroundBox.setPivotY(mHeaderHeightPixels);
        int gapFillDistance = (int) (mHeaderTopClearance * GAP_FILL_DISTANCE_MULTIPLIER);
        boolean showGapFill = !mHasPhoto || (scrollY > (mPhotoHeightPixels - gapFillDistance));

        //Log.d(TAG, "scrollY: " + scrollY);
        //Log.d(TAG, "mPhotoHeightPixels: " + mPhotoHeightPixels);
        //Log.d(TAG, "!mHasPhoto (scrollY > (mPhotoHeightPixels - gapFillDistance) mGapFillShown showGapFill: " + !mHasPhoto + " " + (scrollY > (mPhotoHeightPixels - gapFillDistance)) + " " + mGapFillShown + " " + showGapFill);
        float desiredHeaderScaleY = showGapFill ?
                ((mHeaderHeightPixels + gapFillDistance + 1) * 1.0f / mHeaderHeightPixels)
                : 1.0f;

        if (!Float.isInfinite(desiredHeaderScaleY)) {
            if (!mHasPhoto) {
                mHeaderBackgroundBox.setScaleY(desiredHeaderScaleY);
            }
            else if (mGapFillShown != showGapFill) {

                //Log.d(TAG, "desiredHeaderScaleY: " + desiredHeaderScaleY);

                mHeaderBackgroundBox.animate()
                        .scaleY(desiredHeaderScaleY)
                        .setInterpolator(new DecelerateInterpolator(2.0f))
                        .setDuration(250)
                        .start();
            }
        }
        mGapFillShown = showGapFill;
        //mHeaderShadow.setVisibility(View.VISIBLE);

        if (mHeaderTopClearance != 0) {
            // Fill the gap between status bar and header bar with color
            float gapFillProgress = Math.min(Math.max(AppTools.getProgress(scrollY,
                    mPhotoHeightPixels - mHeaderTopClearance * 2,
                    mPhotoHeightPixels - mHeaderTopClearance), 0), 1);

            mHeaderShadow.setAlpha(gapFillProgress);

        }

        // Move background photo (parallax effect)
        mPhotoViewContainer.setTranslationY(scrollY * 0.5f);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRootScrollView == null) {
            return;
        }

        ViewTreeObserver vto = mRootScrollView.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.removeOnGlobalLayoutListener(mGlobalLayoutListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRequestQueue != null)
            mRequestQueue.cancelAll(TAG);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mRootScrollView != null) {
            outState.putInt(SCROLL_VIEW_X, mRootScrollView.getScrollX());
            outState.putInt(SCROLL_VIEW_Y, mRootScrollView.getScrollY());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.event_detail_menu, menu);

        if (isAgendaAvailable) {
            MenuItem menuItem = menu.findItem(R.id.view_agenda);
            menuItem.setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.view_agenda:
                Intent intent = new Intent(getActivity(), EventAgendaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("event_title", event.getTitle());
                bundle.putString("content", event.getAgendaContent());
                intent.putExtras(bundle);
                getActivity().startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap map) {

        if (mMap == null)
            mMap = map;
    }

    @Override
    public void onTouch() {
        mRootScrollView.requestDisallowInterceptTouchEvent(true);
    }

    private class EventJsonObjectParserAsyncTask extends AsyncTask<JSONObject, Void, Event> {

        @Override
        protected Event doInBackground(JSONObject... jsonObjects) {

            Event event = new Event();

            if (jsonObjects.length > 0) {
                JSONObject eventJsonObject = jsonObjects[0];
                try {
                    event = JSONParser.parseEventJsonObject(eventJsonObject);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return event;
        }

        @Override
        protected void onPostExecute(Event event) {
            setEventData(event);
        }
    }
}
