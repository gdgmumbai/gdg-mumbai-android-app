package org.gdgmumbai.events;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.gdgmumbai.R;
import org.gdgmumbai.common.Session;
import org.gdgmumbai.utils.AppTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ammar Githam on 19-04-2015.
 */
public class SessionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_SESSION = 1;

    private static final String TAG = "SessionsAdapter";

    private Map<Integer, List<Session>> mSessionsHeaderMap;
    private ImageLoader mImageLoader;

    public SessionsAdapter(ArrayList<Session> sessions, ImageLoader mImageLoader) {

        this.mImageLoader = mImageLoader;
        mSessionsHeaderMap = createSessionsHeaderMap(sessions);
    }

    private static Map<Integer, List<Session>> createSessionsHeaderMap(List<Session> sessions) {

        HashMap<Integer, List<Session>> map = new HashMap<>();

        for (Session session : sessions) {
            int headerId = session.getHeaderId();

            if (map.get(Integer.valueOf(headerId)) == null) {
                ArrayList<Session> tempSessions = new ArrayList<>();
                tempSessions.add(session);
                map.put(headerId, tempSessions);
            }
            else {
                map.get(Integer.valueOf(headerId)).add(session);
            }
        }

        return map;
    }

    @Override
    public int getItemViewType(int position) {

        if (isHeaderPosition(position)) {
            return VIEW_TYPE_HEADER;
        }

        return VIEW_TYPE_SESSION;
    }

    private boolean isHeaderPosition(int position) {

        if (position == 0)
            return true;

        Set<Integer> headerIdSet = mSessionsHeaderMap.keySet();

        int tempPosition = 0;

        for (int headerId : headerIdSet) {

            tempPosition += mSessionsHeaderMap.get(headerId).size() + 1;

            if (position == tempPosition)
                return true;
        }

        return false;
    }

    private String getHeaderTextAtPosition(int position) {

        int tempPosition = 0;

        Set<Integer> headerIdSet = mSessionsHeaderMap.keySet();

        for (int headerId : headerIdSet) {

            if (tempPosition == position) {
                return mSessionsHeaderMap.get(headerId).get(0).getHeader();
            }
            else {
                tempPosition++;
                tempPosition += mSessionsHeaderMap.get(headerId).size();
            }
        }

        return "";
    }

    private Session getSessionAtPosition(int position) {

        int tempPosition = 0;

        Set<Integer> headerIdSet = mSessionsHeaderMap.keySet();

        for (int headerId : headerIdSet) {

            int headerPosition = tempPosition;
            tempPosition += mSessionsHeaderMap.get(headerId).size();

            if (position <= tempPosition) {
                List<Session> sessions = mSessionsHeaderMap.get(headerId);
                int index = position - headerPosition - 1;
                return sessions.get(index);
            }

            tempPosition++;
        }

        return new Session();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_HEADER:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_agenda_header, parent, false);
                return new SessionsHeaderViewHolder(headerView);
            default:
            case VIEW_TYPE_SESSION:
                View sessionView = LayoutInflater.from(parent.getContext()).inflate(R.layout.session_list_item, parent, false);

                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                //   sessionView.findViewById(R.id.bottom_shadow).setVisibility(View.GONE);

                return new SessionViewHolder(sessionView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (isHeaderPosition(position)) {
            SessionsHeaderViewHolder headerViewHolder = (SessionsHeaderViewHolder) holder;
            headerViewHolder.header.setText(getHeaderTextAtPosition(position));
        }
        else {
            SessionViewHolder sessionViewHolder = (SessionViewHolder) holder;
            Session session = getSessionAtPosition(position);

            if (AppTools.isNullOrWhitespace(session.getTime()))
                sessionViewHolder.time.setVisibility(View.GONE);
            else {
                sessionViewHolder.time.setText(session.getTime());
                sessionViewHolder.time.setVisibility(View.VISIBLE);
            }

            if (AppTools.isNullOrWhitespace(session.getTopic()))
                sessionViewHolder.topic.setVisibility(View.GONE);
            else {
                sessionViewHolder.topic.setText(session.getTopic());
                sessionViewHolder.topic.setVisibility(View.VISIBLE);
            }

            if (AppTools.isNullOrWhitespace(session.getDescription()))
                sessionViewHolder.description.setVisibility(View.GONE);
            else {
                sessionViewHolder.description.setText(session.getDescription());
                sessionViewHolder.description.setVisibility(View.VISIBLE);
            }

            if (AppTools.isNullOrWhitespace(session.getSpeaker().getName()))
                sessionViewHolder.speakerName.setVisibility(View.GONE);
            else {
                sessionViewHolder.speakerName.setText(session.getSpeaker().getName());
                sessionViewHolder.speakerName.setVisibility(View.VISIBLE);
            }

            if (AppTools.isNullOrWhitespace(session.getSpeaker().getImageURL()))
                sessionViewHolder.speakerImage.setVisibility(View.GONE);
            else {
                sessionViewHolder.speakerImage.setImageUrl(session.getSpeaker().getImageURL(), mImageLoader);
                sessionViewHolder.speakerImage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {

        int totalItems = 0;

        Set<Integer> headerIdSet = mSessionsHeaderMap.keySet();

        for (int headerId : headerIdSet) {
            totalItems++;
            totalItems += mSessionsHeaderMap.get(headerId).size();
        }

        return totalItems;
    }

    public void updateData(ArrayList<Session> sessions) {
        mSessionsHeaderMap = createSessionsHeaderMap(sessions);
    }

    private class SessionViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        TextView topic;
        TextView description;
        NetworkImageView speakerImage;
        TextView speakerName;


        SessionViewHolder(View itemView) {
            super(itemView);

            time = (TextView) itemView.findViewById(R.id.time);
            topic = (TextView) itemView.findViewById(R.id.topic);
            description = (TextView) itemView.findViewById(R.id.description);
            speakerName = (TextView) itemView.findViewById(R.id.speaker_name);
            speakerImage = (NetworkImageView) itemView.findViewById(R.id.speaker_image);
        }
    }

    private class SessionsHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView header;

        SessionsHeaderViewHolder(View itemView) {
            super(itemView);
            header = (TextView) itemView;
        }
    }
}
